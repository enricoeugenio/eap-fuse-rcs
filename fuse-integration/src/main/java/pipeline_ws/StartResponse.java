
package pipeline_ws;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import it.rcs.enfinity.rcs_bc_soa.capi.soa.xsd.NewOfferResponse;


/**
 * <p>Classe Java per anonymous complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="return" type="{http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd}NewOfferResponse" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "_return"
})
@XmlRootElement(name = "StartResponse")
public class StartResponse {

    @XmlElementRef(name = "return", namespace = "http://pipeline_ws", type = JAXBElement.class, required = false)
    protected JAXBElement<NewOfferResponse> _return;

    /**
     * Recupera il valore della proprietÓ return.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link NewOfferResponse }{@code >}
     *     
     */
    public JAXBElement<NewOfferResponse> getReturn() {
        return _return;
    }

    /**
     * Imposta il valore della proprietÓ return.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link NewOfferResponse }{@code >}
     *     
     */
    public void setReturn(JAXBElement<NewOfferResponse> value) {
        this._return = value;
    }

}

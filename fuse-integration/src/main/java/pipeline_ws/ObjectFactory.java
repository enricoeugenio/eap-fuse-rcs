
package pipeline_ws;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;
import it.rcs.enfinity.rcs_bc_soa.capi.soa.xsd.NewOfferResponse;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the pipeline_ws package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _StartResponseReturn_QNAME = new QName("http://pipeline_ws", "return");
    private final static QName _ExceptionException_QNAME = new QName("http://pipeline_ws", "Exception");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: pipeline_ws
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Start }
     * 
     */
    public Start createStart() {
        return new Start();
    }

    /**
     * Create an instance of {@link StartResponse }
     * 
     */
    public StartResponse createStartResponse() {
        return new StartResponse();
    }

    /**
     * Create an instance of {@link Exception }
     * 
     */
    public Exception createException() {
        return new Exception();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link NewOfferResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://pipeline_ws", name = "return", scope = StartResponse.class)
    public JAXBElement<NewOfferResponse> createStartResponseReturn(NewOfferResponse value) {
        return new JAXBElement<NewOfferResponse>(_StartResponseReturn_QNAME, NewOfferResponse.class, StartResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Object }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://pipeline_ws", name = "Exception", scope = Exception.class)
    public JAXBElement<Object> createExceptionException(Object value) {
        return new JAXBElement<Object>(_ExceptionException_QNAME, Object.class, Exception.class, value);
    }

}

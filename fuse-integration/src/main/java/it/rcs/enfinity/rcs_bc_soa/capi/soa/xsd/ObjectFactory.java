
package it.rcs.enfinity.rcs_bc_soa.capi.soa.xsd;

import java.math.BigDecimal;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.namespace.QName;
import it.rcs.enfinity.rcs_bc_soa.capi.soa.displayinfo.xsd.DisplayInfo;
import it.rcs.enfinity.rcs_bc_soa.capi.soa.editorialentity.xsd.EditorialEntity;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the it.rcs.enfinity.rcs_bc_soa.capi.soa.xsd package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _TipoAbbonamentoTipoAbbonamentoName_QNAME = new QName("http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", "tipoAbbonamentoName");
    private final static QName _PromotionChainIntervalRecurrenceNumber_QNAME = new QName("http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", "intervalRecurrenceNumber");
    private final static QName _PromotionChainScivoloNoDisable_QNAME = new QName("http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", "scivoloNoDisable");
    private final static QName _PromotionChainScivoloRateale_QNAME = new QName("http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", "scivoloRateale");
    private final static QName _ProductAccessNumber_QNAME = new QName("http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", "accessNumber");
    private final static QName _ProductArrayOfPromotionChain_QNAME = new QName("http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", "arrayOfPromotionChain");
    private final static QName _ProductCopiesNumberOnFriday_QNAME = new QName("http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", "copiesNumberOnFriday");
    private final static QName _ProductCopiesNumberOnMonday_QNAME = new QName("http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", "copiesNumberOnMonday");
    private final static QName _ProductCopiesNumberOnSaturday_QNAME = new QName("http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", "copiesNumberOnSaturday");
    private final static QName _ProductCopiesNumberOnSunday_QNAME = new QName("http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", "copiesNumberOnSunday");
    private final static QName _ProductCopiesNumberOnThursday_QNAME = new QName("http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", "copiesNumberOnThursday");
    private final static QName _ProductCopiesNumberOnTuesday_QNAME = new QName("http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", "copiesNumberOnTuesday");
    private final static QName _ProductCopiesNumberOnWednesday_QNAME = new QName("http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", "copiesNumberOnWednesday");
    private final static QName _ProductDataFruizioneFrom_QNAME = new QName("http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", "dataFruizioneFrom");
    private final static QName _ProductDataFruizioneTo_QNAME = new QName("http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", "dataFruizioneTo");
    private final static QName _ProductDigitalProduct_QNAME = new QName("http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", "digitalProduct");
    private final static QName _ProductDiscountAmount_QNAME = new QName("http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", "discountAmount");
    private final static QName _ProductDiscountPercent_QNAME = new QName("http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", "discountPercent");
    private final static QName _ProductDiscountType_QNAME = new QName("http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", "discountType");
    private final static QName _ProductEditorialEntity_QNAME = new QName("http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", "editorialEntity");
    private final static QName _ProductFridayPrice_QNAME = new QName("http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", "fridayPrice");
    private final static QName _ProductFridayUse_QNAME = new QName("http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", "fridayUse");
    private final static QName _ProductIsTrial_QNAME = new QName("http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", "isTrial");
    private final static QName _ProductIssueNumber_QNAME = new QName("http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", "issueNumber");
    private final static QName _ProductIssueYear_QNAME = new QName("http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", "issueYear");
    private final static QName _ProductMondayPrice_QNAME = new QName("http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", "mondayPrice");
    private final static QName _ProductMondayUse_QNAME = new QName("http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", "mondayUse");
    private final static QName _ProductPricingScheme_QNAME = new QName("http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", "pricingScheme");
    private final static QName _ProductProductOfferType_QNAME = new QName("http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", "productOfferType");
    private final static QName _ProductProductPeriodicity_QNAME = new QName("http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", "productPeriodicity");
    private final static QName _ProductProductTipology_QNAME = new QName("http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", "productTipology");
    private final static QName _ProductQuantity_QNAME = new QName("http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", "quantity");
    private final static QName _ProductRecurrentPriceDiscountType_QNAME = new QName("http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", "recurrentPriceDiscountType");
    private final static QName _ProductRecurrentPriceGross_QNAME = new QName("http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", "recurrentPriceGross");
    private final static QName _ProductRecurrentPriceNet_QNAME = new QName("http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", "recurrentPriceNet");
    private final static QName _ProductRecurrentPricePercentDiscount_QNAME = new QName("http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", "recurrentPricePercentDiscount");
    private final static QName _ProductRecurringPeriodicity_QNAME = new QName("http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", "recurringPeriodicity");
    private final static QName _ProductSaturdayPrice_QNAME = new QName("http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", "saturdayPrice");
    private final static QName _ProductSaturdayUse_QNAME = new QName("http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", "saturdayUse");
    private final static QName _ProductScivoloType_QNAME = new QName("http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", "scivoloType");
    private final static QName _ProductShippingCost_QNAME = new QName("http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", "shippingCost");
    private final static QName _ProductSubscriptionDurationCopies_QNAME = new QName("http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", "subscriptionDurationCopies");
    private final static QName _ProductSubscriptionDurationDays_QNAME = new QName("http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", "subscriptionDurationDays");
    private final static QName _ProductSubscriptionDurationFormat_QNAME = new QName("http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", "subscriptionDurationFormat");
    private final static QName _ProductSubscriptionDurationMonths_QNAME = new QName("http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", "subscriptionDurationMonths");
    private final static QName _ProductSubscriptionDurationWeek_QNAME = new QName("http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", "subscriptionDurationWeek");
    private final static QName _ProductSubscriptionRecurrencesNumber_QNAME = new QName("http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", "subscriptionRecurrencesNumber");
    private final static QName _ProductSundayPrice_QNAME = new QName("http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", "sundayPrice");
    private final static QName _ProductSundayUse_QNAME = new QName("http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", "sundayUse");
    private final static QName _ProductThursdayPrice_QNAME = new QName("http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", "thursdayPrice");
    private final static QName _ProductThursdayUse_QNAME = new QName("http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", "thursdayUse");
    private final static QName _ProductTuesdayPrice_QNAME = new QName("http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", "tuesdayPrice");
    private final static QName _ProductTuesdayUse_QNAME = new QName("http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", "tuesdayUse");
    private final static QName _ProductWednesdayPrice_QNAME = new QName("http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", "wednesdayPrice");
    private final static QName _ProductWednesdayUse_QNAME = new QName("http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", "wednesdayUse");
    private final static QName _NewOfferResponseReasonDescription_QNAME = new QName("http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", "reasonDescription");
    private final static QName _NewOfferResponseReasonTitle_QNAME = new QName("http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", "reasonTitle");
    private final static QName _OfferArrayOfTipoAbbonamento_QNAME = new QName("http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", "arrayOfTipoAbbonamento");
    private final static QName _OfferBusinessUnit_QNAME = new QName("http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", "businessUnit");
    private final static QName _OfferDisplayInfo_QNAME = new QName("http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", "displayInfo");
    private final static QName _OfferFromNumberConsumer_QNAME = new QName("http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", "fromNumberConsumer");
    private final static QName _OfferGiftCode_QNAME = new QName("http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", "giftCode");
    private final static QName _OfferGiftDescription_QNAME = new QName("http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", "giftDescription");
    private final static QName _OfferGiftFlag_QNAME = new QName("http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", "giftFlag");
    private final static QName _OfferGratisFirstPurchase_QNAME = new QName("http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", "gratisFirstPurchase");
    private final static QName _OfferIsCumulable_QNAME = new QName("http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", "isCumulable");
    private final static QName _OfferIsExecutive_QNAME = new QName("http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", "isExecutive");
    private final static QName _OfferLimiteGratis_QNAME = new QName("http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", "limiteGratis");
    private final static QName _OfferNestedOffer_QNAME = new QName("http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", "nestedOffer");
    private final static QName _OfferNestedOfferVersion_QNAME = new QName("http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", "nestedOfferVersion");
    private final static QName _OfferNomePartner_QNAME = new QName("http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", "nomePartner");
    private final static QName _OfferOfferLanguage_QNAME = new QName("http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", "offerLanguage");
    private final static QName _OfferOfferStatus_QNAME = new QName("http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", "offerStatus");
    private final static QName _OfferPartnerId_QNAME = new QName("http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", "partnerId");
    private final static QName _OfferPersistentPromoCode_QNAME = new QName("http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", "persistentPromoCode");
    private final static QName _OfferPromotionalCodeFlag_QNAME = new QName("http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", "promotionalCodeFlag");
    private final static QName _OfferShippingCostTotal_QNAME = new QName("http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", "shippingCostTotal");
    private final static QName _OfferSingleInstance_QNAME = new QName("http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", "singleInstance");
    private final static QName _OfferToNumberConsumer_QNAME = new QName("http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", "toNumberConsumer");
    private final static QName _OfferVodafoneAuthomaticBilling_QNAME = new QName("http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", "vodafoneAuthomaticBilling");
    private final static QName _OfferMPaymentId_QNAME = new QName("http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", "mPaymentId");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: it.rcs.enfinity.rcs_bc_soa.capi.soa.xsd
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Offer }
     * 
     */
    public Offer createOffer() {
        return new Offer();
    }

    /**
     * Create an instance of {@link NewOfferResponse }
     * 
     */
    public NewOfferResponse createNewOfferResponse() {
        return new NewOfferResponse();
    }

    /**
     * Create an instance of {@link ArrayOfPaymentMethod }
     * 
     */
    public ArrayOfPaymentMethod createArrayOfPaymentMethod() {
        return new ArrayOfPaymentMethod();
    }

    /**
     * Create an instance of {@link PaymentMethod }
     * 
     */
    public PaymentMethod createPaymentMethod() {
        return new PaymentMethod();
    }

    /**
     * Create an instance of {@link ArrayOfProduct }
     * 
     */
    public ArrayOfProduct createArrayOfProduct() {
        return new ArrayOfProduct();
    }

    /**
     * Create an instance of {@link Product }
     * 
     */
    public Product createProduct() {
        return new Product();
    }

    /**
     * Create an instance of {@link ArrayOfPromotionChain }
     * 
     */
    public ArrayOfPromotionChain createArrayOfPromotionChain() {
        return new ArrayOfPromotionChain();
    }

    /**
     * Create an instance of {@link PromotionChain }
     * 
     */
    public PromotionChain createPromotionChain() {
        return new PromotionChain();
    }

    /**
     * Create an instance of {@link ArrayOfTipoAbbonamento }
     * 
     */
    public ArrayOfTipoAbbonamento createArrayOfTipoAbbonamento() {
        return new ArrayOfTipoAbbonamento();
    }

    /**
     * Create an instance of {@link TipoAbbonamento }
     * 
     */
    public TipoAbbonamento createTipoAbbonamento() {
        return new TipoAbbonamento();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", name = "tipoAbbonamentoName", scope = TipoAbbonamento.class)
    public JAXBElement<String> createTipoAbbonamentoTipoAbbonamentoName(String value) {
        return new JAXBElement<String>(_TipoAbbonamentoTipoAbbonamentoName_QNAME, String.class, TipoAbbonamento.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", name = "intervalRecurrenceNumber", scope = PromotionChain.class)
    public JAXBElement<Integer> createPromotionChainIntervalRecurrenceNumber(Integer value) {
        return new JAXBElement<Integer>(_PromotionChainIntervalRecurrenceNumber_QNAME, Integer.class, PromotionChain.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", name = "scivoloNoDisable", scope = PromotionChain.class)
    public JAXBElement<Boolean> createPromotionChainScivoloNoDisable(Boolean value) {
        return new JAXBElement<Boolean>(_PromotionChainScivoloNoDisable_QNAME, Boolean.class, PromotionChain.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", name = "scivoloRateale", scope = PromotionChain.class)
    public JAXBElement<Boolean> createPromotionChainScivoloRateale(Boolean value) {
        return new JAXBElement<Boolean>(_PromotionChainScivoloRateale_QNAME, Boolean.class, PromotionChain.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", name = "accessNumber", scope = Product.class)
    public JAXBElement<Integer> createProductAccessNumber(Integer value) {
        return new JAXBElement<Integer>(_ProductAccessNumber_QNAME, Integer.class, Product.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfPromotionChain }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", name = "arrayOfPromotionChain", scope = Product.class)
    public JAXBElement<ArrayOfPromotionChain> createProductArrayOfPromotionChain(ArrayOfPromotionChain value) {
        return new JAXBElement<ArrayOfPromotionChain>(_ProductArrayOfPromotionChain_QNAME, ArrayOfPromotionChain.class, Product.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", name = "copiesNumberOnFriday", scope = Product.class)
    public JAXBElement<Integer> createProductCopiesNumberOnFriday(Integer value) {
        return new JAXBElement<Integer>(_ProductCopiesNumberOnFriday_QNAME, Integer.class, Product.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", name = "copiesNumberOnMonday", scope = Product.class)
    public JAXBElement<Integer> createProductCopiesNumberOnMonday(Integer value) {
        return new JAXBElement<Integer>(_ProductCopiesNumberOnMonday_QNAME, Integer.class, Product.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", name = "copiesNumberOnSaturday", scope = Product.class)
    public JAXBElement<Integer> createProductCopiesNumberOnSaturday(Integer value) {
        return new JAXBElement<Integer>(_ProductCopiesNumberOnSaturday_QNAME, Integer.class, Product.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", name = "copiesNumberOnSunday", scope = Product.class)
    public JAXBElement<Integer> createProductCopiesNumberOnSunday(Integer value) {
        return new JAXBElement<Integer>(_ProductCopiesNumberOnSunday_QNAME, Integer.class, Product.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", name = "copiesNumberOnThursday", scope = Product.class)
    public JAXBElement<Integer> createProductCopiesNumberOnThursday(Integer value) {
        return new JAXBElement<Integer>(_ProductCopiesNumberOnThursday_QNAME, Integer.class, Product.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", name = "copiesNumberOnTuesday", scope = Product.class)
    public JAXBElement<Integer> createProductCopiesNumberOnTuesday(Integer value) {
        return new JAXBElement<Integer>(_ProductCopiesNumberOnTuesday_QNAME, Integer.class, Product.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", name = "copiesNumberOnWednesday", scope = Product.class)
    public JAXBElement<Integer> createProductCopiesNumberOnWednesday(Integer value) {
        return new JAXBElement<Integer>(_ProductCopiesNumberOnWednesday_QNAME, Integer.class, Product.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", name = "dataFruizioneFrom", scope = Product.class)
    public JAXBElement<XMLGregorianCalendar> createProductDataFruizioneFrom(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_ProductDataFruizioneFrom_QNAME, XMLGregorianCalendar.class, Product.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", name = "dataFruizioneTo", scope = Product.class)
    public JAXBElement<XMLGregorianCalendar> createProductDataFruizioneTo(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_ProductDataFruizioneTo_QNAME, XMLGregorianCalendar.class, Product.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", name = "digitalProduct", scope = Product.class)
    public JAXBElement<String> createProductDigitalProduct(String value) {
        return new JAXBElement<String>(_ProductDigitalProduct_QNAME, String.class, Product.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", name = "discountAmount", scope = Product.class)
    public JAXBElement<BigDecimal> createProductDiscountAmount(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_ProductDiscountAmount_QNAME, BigDecimal.class, Product.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", name = "discountPercent", scope = Product.class)
    public JAXBElement<BigDecimal> createProductDiscountPercent(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_ProductDiscountPercent_QNAME, BigDecimal.class, Product.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", name = "discountType", scope = Product.class)
    public JAXBElement<String> createProductDiscountType(String value) {
        return new JAXBElement<String>(_ProductDiscountType_QNAME, String.class, Product.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EditorialEntity }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", name = "editorialEntity", scope = Product.class)
    public JAXBElement<EditorialEntity> createProductEditorialEntity(EditorialEntity value) {
        return new JAXBElement<EditorialEntity>(_ProductEditorialEntity_QNAME, EditorialEntity.class, Product.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", name = "fridayPrice", scope = Product.class)
    public JAXBElement<BigDecimal> createProductFridayPrice(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_ProductFridayPrice_QNAME, BigDecimal.class, Product.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", name = "fridayUse", scope = Product.class)
    public JAXBElement<Boolean> createProductFridayUse(Boolean value) {
        return new JAXBElement<Boolean>(_ProductFridayUse_QNAME, Boolean.class, Product.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", name = "isTrial", scope = Product.class)
    public JAXBElement<Boolean> createProductIsTrial(Boolean value) {
        return new JAXBElement<Boolean>(_ProductIsTrial_QNAME, Boolean.class, Product.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", name = "issueNumber", scope = Product.class)
    public JAXBElement<String> createProductIssueNumber(String value) {
        return new JAXBElement<String>(_ProductIssueNumber_QNAME, String.class, Product.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", name = "issueYear", scope = Product.class)
    public JAXBElement<String> createProductIssueYear(String value) {
        return new JAXBElement<String>(_ProductIssueYear_QNAME, String.class, Product.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", name = "mondayPrice", scope = Product.class)
    public JAXBElement<BigDecimal> createProductMondayPrice(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_ProductMondayPrice_QNAME, BigDecimal.class, Product.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", name = "mondayUse", scope = Product.class)
    public JAXBElement<Boolean> createProductMondayUse(Boolean value) {
        return new JAXBElement<Boolean>(_ProductMondayUse_QNAME, Boolean.class, Product.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", name = "pricingScheme", scope = Product.class)
    public JAXBElement<String> createProductPricingScheme(String value) {
        return new JAXBElement<String>(_ProductPricingScheme_QNAME, String.class, Product.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", name = "productOfferType", scope = Product.class)
    public JAXBElement<String> createProductProductOfferType(String value) {
        return new JAXBElement<String>(_ProductProductOfferType_QNAME, String.class, Product.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", name = "productPeriodicity", scope = Product.class)
    public JAXBElement<String> createProductProductPeriodicity(String value) {
        return new JAXBElement<String>(_ProductProductPeriodicity_QNAME, String.class, Product.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", name = "productTipology", scope = Product.class)
    public JAXBElement<String> createProductProductTipology(String value) {
        return new JAXBElement<String>(_ProductProductTipology_QNAME, String.class, Product.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", name = "quantity", scope = Product.class)
    public JAXBElement<Integer> createProductQuantity(Integer value) {
        return new JAXBElement<Integer>(_ProductQuantity_QNAME, Integer.class, Product.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", name = "recurrentPriceDiscountType", scope = Product.class)
    public JAXBElement<String> createProductRecurrentPriceDiscountType(String value) {
        return new JAXBElement<String>(_ProductRecurrentPriceDiscountType_QNAME, String.class, Product.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", name = "recurrentPriceGross", scope = Product.class)
    public JAXBElement<BigDecimal> createProductRecurrentPriceGross(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_ProductRecurrentPriceGross_QNAME, BigDecimal.class, Product.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", name = "recurrentPriceNet", scope = Product.class)
    public JAXBElement<BigDecimal> createProductRecurrentPriceNet(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_ProductRecurrentPriceNet_QNAME, BigDecimal.class, Product.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", name = "recurrentPricePercentDiscount", scope = Product.class)
    public JAXBElement<BigDecimal> createProductRecurrentPricePercentDiscount(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_ProductRecurrentPricePercentDiscount_QNAME, BigDecimal.class, Product.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", name = "recurringPeriodicity", scope = Product.class)
    public JAXBElement<String> createProductRecurringPeriodicity(String value) {
        return new JAXBElement<String>(_ProductRecurringPeriodicity_QNAME, String.class, Product.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", name = "saturdayPrice", scope = Product.class)
    public JAXBElement<BigDecimal> createProductSaturdayPrice(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_ProductSaturdayPrice_QNAME, BigDecimal.class, Product.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", name = "saturdayUse", scope = Product.class)
    public JAXBElement<Boolean> createProductSaturdayUse(Boolean value) {
        return new JAXBElement<Boolean>(_ProductSaturdayUse_QNAME, Boolean.class, Product.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", name = "scivoloType", scope = Product.class)
    public JAXBElement<String> createProductScivoloType(String value) {
        return new JAXBElement<String>(_ProductScivoloType_QNAME, String.class, Product.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", name = "shippingCost", scope = Product.class)
    public JAXBElement<BigDecimal> createProductShippingCost(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_ProductShippingCost_QNAME, BigDecimal.class, Product.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", name = "subscriptionDurationCopies", scope = Product.class)
    public JAXBElement<Integer> createProductSubscriptionDurationCopies(Integer value) {
        return new JAXBElement<Integer>(_ProductSubscriptionDurationCopies_QNAME, Integer.class, Product.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", name = "subscriptionDurationDays", scope = Product.class)
    public JAXBElement<Integer> createProductSubscriptionDurationDays(Integer value) {
        return new JAXBElement<Integer>(_ProductSubscriptionDurationDays_QNAME, Integer.class, Product.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", name = "subscriptionDurationFormat", scope = Product.class)
    public JAXBElement<String> createProductSubscriptionDurationFormat(String value) {
        return new JAXBElement<String>(_ProductSubscriptionDurationFormat_QNAME, String.class, Product.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", name = "subscriptionDurationMonths", scope = Product.class)
    public JAXBElement<Integer> createProductSubscriptionDurationMonths(Integer value) {
        return new JAXBElement<Integer>(_ProductSubscriptionDurationMonths_QNAME, Integer.class, Product.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", name = "subscriptionDurationWeek", scope = Product.class)
    public JAXBElement<Integer> createProductSubscriptionDurationWeek(Integer value) {
        return new JAXBElement<Integer>(_ProductSubscriptionDurationWeek_QNAME, Integer.class, Product.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", name = "subscriptionRecurrencesNumber", scope = Product.class)
    public JAXBElement<Integer> createProductSubscriptionRecurrencesNumber(Integer value) {
        return new JAXBElement<Integer>(_ProductSubscriptionRecurrencesNumber_QNAME, Integer.class, Product.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", name = "sundayPrice", scope = Product.class)
    public JAXBElement<BigDecimal> createProductSundayPrice(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_ProductSundayPrice_QNAME, BigDecimal.class, Product.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", name = "sundayUse", scope = Product.class)
    public JAXBElement<Boolean> createProductSundayUse(Boolean value) {
        return new JAXBElement<Boolean>(_ProductSundayUse_QNAME, Boolean.class, Product.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", name = "thursdayPrice", scope = Product.class)
    public JAXBElement<BigDecimal> createProductThursdayPrice(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_ProductThursdayPrice_QNAME, BigDecimal.class, Product.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", name = "thursdayUse", scope = Product.class)
    public JAXBElement<Boolean> createProductThursdayUse(Boolean value) {
        return new JAXBElement<Boolean>(_ProductThursdayUse_QNAME, Boolean.class, Product.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", name = "tuesdayPrice", scope = Product.class)
    public JAXBElement<BigDecimal> createProductTuesdayPrice(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_ProductTuesdayPrice_QNAME, BigDecimal.class, Product.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", name = "tuesdayUse", scope = Product.class)
    public JAXBElement<Boolean> createProductTuesdayUse(Boolean value) {
        return new JAXBElement<Boolean>(_ProductTuesdayUse_QNAME, Boolean.class, Product.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", name = "wednesdayPrice", scope = Product.class)
    public JAXBElement<BigDecimal> createProductWednesdayPrice(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_ProductWednesdayPrice_QNAME, BigDecimal.class, Product.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", name = "wednesdayUse", scope = Product.class)
    public JAXBElement<Boolean> createProductWednesdayUse(Boolean value) {
        return new JAXBElement<Boolean>(_ProductWednesdayUse_QNAME, Boolean.class, Product.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", name = "reasonDescription", scope = NewOfferResponse.class)
    public JAXBElement<String> createNewOfferResponseReasonDescription(String value) {
        return new JAXBElement<String>(_NewOfferResponseReasonDescription_QNAME, String.class, NewOfferResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", name = "reasonTitle", scope = NewOfferResponse.class)
    public JAXBElement<String> createNewOfferResponseReasonTitle(String value) {
        return new JAXBElement<String>(_NewOfferResponseReasonTitle_QNAME, String.class, NewOfferResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfTipoAbbonamento }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", name = "arrayOfTipoAbbonamento", scope = Offer.class)
    public JAXBElement<ArrayOfTipoAbbonamento> createOfferArrayOfTipoAbbonamento(ArrayOfTipoAbbonamento value) {
        return new JAXBElement<ArrayOfTipoAbbonamento>(_OfferArrayOfTipoAbbonamento_QNAME, ArrayOfTipoAbbonamento.class, Offer.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", name = "businessUnit", scope = Offer.class)
    public JAXBElement<String> createOfferBusinessUnit(String value) {
        return new JAXBElement<String>(_OfferBusinessUnit_QNAME, String.class, Offer.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DisplayInfo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", name = "displayInfo", scope = Offer.class)
    public JAXBElement<DisplayInfo> createOfferDisplayInfo(DisplayInfo value) {
        return new JAXBElement<DisplayInfo>(_OfferDisplayInfo_QNAME, DisplayInfo.class, Offer.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", name = "fromNumberConsumer", scope = Offer.class)
    public JAXBElement<Integer> createOfferFromNumberConsumer(Integer value) {
        return new JAXBElement<Integer>(_OfferFromNumberConsumer_QNAME, Integer.class, Offer.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", name = "giftCode", scope = Offer.class)
    public JAXBElement<String> createOfferGiftCode(String value) {
        return new JAXBElement<String>(_OfferGiftCode_QNAME, String.class, Offer.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", name = "giftDescription", scope = Offer.class)
    public JAXBElement<String> createOfferGiftDescription(String value) {
        return new JAXBElement<String>(_OfferGiftDescription_QNAME, String.class, Offer.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", name = "giftFlag", scope = Offer.class)
    public JAXBElement<Boolean> createOfferGiftFlag(Boolean value) {
        return new JAXBElement<Boolean>(_OfferGiftFlag_QNAME, Boolean.class, Offer.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", name = "gratisFirstPurchase", scope = Offer.class)
    public JAXBElement<String> createOfferGratisFirstPurchase(String value) {
        return new JAXBElement<String>(_OfferGratisFirstPurchase_QNAME, String.class, Offer.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", name = "isCumulable", scope = Offer.class)
    public JAXBElement<Boolean> createOfferIsCumulable(Boolean value) {
        return new JAXBElement<Boolean>(_OfferIsCumulable_QNAME, Boolean.class, Offer.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", name = "isExecutive", scope = Offer.class)
    public JAXBElement<Boolean> createOfferIsExecutive(Boolean value) {
        return new JAXBElement<Boolean>(_OfferIsExecutive_QNAME, Boolean.class, Offer.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", name = "limiteGratis", scope = Offer.class)
    public JAXBElement<String> createOfferLimiteGratis(String value) {
        return new JAXBElement<String>(_OfferLimiteGratis_QNAME, String.class, Offer.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", name = "nestedOffer", scope = Offer.class)
    public JAXBElement<String> createOfferNestedOffer(String value) {
        return new JAXBElement<String>(_OfferNestedOffer_QNAME, String.class, Offer.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", name = "nestedOfferVersion", scope = Offer.class)
    public JAXBElement<String> createOfferNestedOfferVersion(String value) {
        return new JAXBElement<String>(_OfferNestedOfferVersion_QNAME, String.class, Offer.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", name = "nomePartner", scope = Offer.class)
    public JAXBElement<String> createOfferNomePartner(String value) {
        return new JAXBElement<String>(_OfferNomePartner_QNAME, String.class, Offer.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", name = "offerLanguage", scope = Offer.class)
    public JAXBElement<String> createOfferOfferLanguage(String value) {
        return new JAXBElement<String>(_OfferOfferLanguage_QNAME, String.class, Offer.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", name = "offerStatus", scope = Offer.class)
    public JAXBElement<String> createOfferOfferStatus(String value) {
        return new JAXBElement<String>(_OfferOfferStatus_QNAME, String.class, Offer.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", name = "partnerId", scope = Offer.class)
    public JAXBElement<String> createOfferPartnerId(String value) {
        return new JAXBElement<String>(_OfferPartnerId_QNAME, String.class, Offer.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", name = "persistentPromoCode", scope = Offer.class)
    public JAXBElement<String> createOfferPersistentPromoCode(String value) {
        return new JAXBElement<String>(_OfferPersistentPromoCode_QNAME, String.class, Offer.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", name = "promotionalCodeFlag", scope = Offer.class)
    public JAXBElement<Boolean> createOfferPromotionalCodeFlag(Boolean value) {
        return new JAXBElement<Boolean>(_OfferPromotionalCodeFlag_QNAME, Boolean.class, Offer.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", name = "shippingCostTotal", scope = Offer.class)
    public JAXBElement<BigDecimal> createOfferShippingCostTotal(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_OfferShippingCostTotal_QNAME, BigDecimal.class, Offer.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", name = "singleInstance", scope = Offer.class)
    public JAXBElement<String> createOfferSingleInstance(String value) {
        return new JAXBElement<String>(_OfferSingleInstance_QNAME, String.class, Offer.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", name = "toNumberConsumer", scope = Offer.class)
    public JAXBElement<Integer> createOfferToNumberConsumer(Integer value) {
        return new JAXBElement<Integer>(_OfferToNumberConsumer_QNAME, Integer.class, Offer.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", name = "vodafoneAuthomaticBilling", scope = Offer.class)
    public JAXBElement<Boolean> createOfferVodafoneAuthomaticBilling(Boolean value) {
        return new JAXBElement<Boolean>(_OfferVodafoneAuthomaticBilling_QNAME, Boolean.class, Offer.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", name = "mPaymentId", scope = Offer.class)
    public JAXBElement<String> createOfferMPaymentId(String value) {
        return new JAXBElement<String>(_OfferMPaymentId_QNAME, String.class, Offer.class, value);
    }

}

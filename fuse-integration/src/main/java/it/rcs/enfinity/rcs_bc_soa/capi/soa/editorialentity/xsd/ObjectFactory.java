
package it.rcs.enfinity.rcs_bc_soa.capi.soa.editorialentity.xsd;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the it.rcs.enfinity.rcs_bc_soa.capi.soa.editorialentity.xsd package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _EditorialEntityContextID_QNAME = new QName("http://editorialEntity.soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", "contextID");
    private final static QName _EditorialEntityMembershipPaywall_QNAME = new QName("http://editorialEntity.soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", "membershipPaywall");
    private final static QName _EditorialEntityName_QNAME = new QName("http://editorialEntity.soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", "name");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: it.rcs.enfinity.rcs_bc_soa.capi.soa.editorialentity.xsd
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link EditorialEntity }
     * 
     */
    public EditorialEntity createEditorialEntity() {
        return new EditorialEntity();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://editorialEntity.soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", name = "contextID", scope = EditorialEntity.class)
    public JAXBElement<String> createEditorialEntityContextID(String value) {
        return new JAXBElement<String>(_EditorialEntityContextID_QNAME, String.class, EditorialEntity.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://editorialEntity.soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", name = "membershipPaywall", scope = EditorialEntity.class)
    public JAXBElement<Boolean> createEditorialEntityMembershipPaywall(Boolean value) {
        return new JAXBElement<Boolean>(_EditorialEntityMembershipPaywall_QNAME, Boolean.class, EditorialEntity.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://editorialEntity.soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", name = "name", scope = EditorialEntity.class)
    public JAXBElement<String> createEditorialEntityName(String value) {
        return new JAXBElement<String>(_EditorialEntityName_QNAME, String.class, EditorialEntity.class, value);
    }

}


package it.rcs.enfinity.rcs_bc_soa.capi.soa.xsd;

import java.math.BigDecimal;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import it.rcs.enfinity.rcs_bc_soa.capi.soa.displayinfo.xsd.DisplayInfo;


/**
 * <p>Classe Java per Offer complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="Offer"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="arrayOfPaymentMethod" type="{http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd}ArrayOfPaymentMethod"/&gt;
 *         &lt;element name="arrayOfProduct" type="{http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd}ArrayOfProduct"/&gt;
 *         &lt;element name="arrayOfTipoAbbonamento" type="{http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd}ArrayOfTipoAbbonamento" minOccurs="0"/&gt;
 *         &lt;element name="businessUnit" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="customerSegmentation" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="discountPercentage" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
 *         &lt;element name="discountPercentageFinal" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
 *         &lt;element name="discountValueFinal" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
 *         &lt;element name="displayInfo" type="{http://displayInfo.soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd}DisplayInfo" minOccurs="0"/&gt;
 *         &lt;element name="fromNumberConsumer" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/&gt;
 *         &lt;element name="giftCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="giftDescription" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="giftFlag" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="gratisFirstPurchase" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="grossPriceOffer" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
 *         &lt;element name="isCumulable" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="isExecutive" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="limiteGratis" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="nestedOffer" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="nestedOfferVersion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="netFinalOfferPrice" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
 *         &lt;element name="netFinalOfferPriceNoIVA" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
 *         &lt;element name="netPriceOffer" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
 *         &lt;element name="nomePartner" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="offerDescription" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="offerId" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="offerLanguage" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="offerName" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="offerStatus" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="offerType" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="offerVersion" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="partnerId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="persistentPromoCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="promotionalCodeFlag" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="shippingCostTotal" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/&gt;
 *         &lt;element name="singleInstance" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="toNumberConsumer" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/&gt;
 *         &lt;element name="validFrom" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *         &lt;element name="validTo" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *         &lt;element name="vodafoneAuthomaticBilling" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="zone" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="mPaymentId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Offer", propOrder = {
    "arrayOfPaymentMethod",
    "arrayOfProduct",
    "arrayOfTipoAbbonamento",
    "businessUnit",
    "customerSegmentation",
    "discountPercentage",
    "discountPercentageFinal",
    "discountValueFinal",
    "displayInfo",
    "fromNumberConsumer",
    "giftCode",
    "giftDescription",
    "giftFlag",
    "gratisFirstPurchase",
    "grossPriceOffer",
    "isCumulable",
    "isExecutive",
    "limiteGratis",
    "nestedOffer",
    "nestedOfferVersion",
    "netFinalOfferPrice",
    "netFinalOfferPriceNoIVA",
    "netPriceOffer",
    "nomePartner",
    "offerDescription",
    "offerId",
    "offerLanguage",
    "offerName",
    "offerStatus",
    "offerType",
    "offerVersion",
    "partnerId",
    "persistentPromoCode",
    "promotionalCodeFlag",
    "shippingCostTotal",
    "singleInstance",
    "toNumberConsumer",
    "validFrom",
    "validTo",
    "vodafoneAuthomaticBilling",
    "zone",
    "mPaymentId"
})
public class Offer {

    @XmlElement(required = true)
    protected ArrayOfPaymentMethod arrayOfPaymentMethod;
    @XmlElement(required = true)
    protected ArrayOfProduct arrayOfProduct;
    @XmlElementRef(name = "arrayOfTipoAbbonamento", namespace = "http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfTipoAbbonamento> arrayOfTipoAbbonamento;
    @XmlElementRef(name = "businessUnit", namespace = "http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", type = JAXBElement.class, required = false)
    protected JAXBElement<String> businessUnit;
    @XmlElement(required = true)
    protected String customerSegmentation;
    @XmlElement(required = true)
    protected BigDecimal discountPercentage;
    @XmlElement(required = true)
    protected BigDecimal discountPercentageFinal;
    @XmlElement(required = true)
    protected BigDecimal discountValueFinal;
    @XmlElementRef(name = "displayInfo", namespace = "http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", type = JAXBElement.class, required = false)
    protected JAXBElement<DisplayInfo> displayInfo;
    @XmlElementRef(name = "fromNumberConsumer", namespace = "http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> fromNumberConsumer;
    @XmlElementRef(name = "giftCode", namespace = "http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", type = JAXBElement.class, required = false)
    protected JAXBElement<String> giftCode;
    @XmlElementRef(name = "giftDescription", namespace = "http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", type = JAXBElement.class, required = false)
    protected JAXBElement<String> giftDescription;
    @XmlElementRef(name = "giftFlag", namespace = "http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> giftFlag;
    @XmlElementRef(name = "gratisFirstPurchase", namespace = "http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", type = JAXBElement.class, required = false)
    protected JAXBElement<String> gratisFirstPurchase;
    @XmlElement(required = true)
    protected BigDecimal grossPriceOffer;
    @XmlElementRef(name = "isCumulable", namespace = "http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> isCumulable;
    @XmlElementRef(name = "isExecutive", namespace = "http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> isExecutive;
    @XmlElementRef(name = "limiteGratis", namespace = "http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", type = JAXBElement.class, required = false)
    protected JAXBElement<String> limiteGratis;
    @XmlElementRef(name = "nestedOffer", namespace = "http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", type = JAXBElement.class, required = false)
    protected JAXBElement<String> nestedOffer;
    @XmlElementRef(name = "nestedOfferVersion", namespace = "http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", type = JAXBElement.class, required = false)
    protected JAXBElement<String> nestedOfferVersion;
    @XmlElement(required = true)
    protected BigDecimal netFinalOfferPrice;
    @XmlElement(required = true)
    protected BigDecimal netFinalOfferPriceNoIVA;
    @XmlElement(required = true)
    protected BigDecimal netPriceOffer;
    @XmlElementRef(name = "nomePartner", namespace = "http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", type = JAXBElement.class, required = false)
    protected JAXBElement<String> nomePartner;
    @XmlElement(required = true)
    protected String offerDescription;
    @XmlElement(required = true)
    protected String offerId;
    @XmlElementRef(name = "offerLanguage", namespace = "http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", type = JAXBElement.class, required = false)
    protected JAXBElement<String> offerLanguage;
    @XmlElement(required = true)
    protected String offerName;
    @XmlElementRef(name = "offerStatus", namespace = "http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", type = JAXBElement.class, required = false)
    protected JAXBElement<String> offerStatus;
    @XmlElement(required = true)
    protected String offerType;
    @XmlElement(required = true)
    protected String offerVersion;
    @XmlElementRef(name = "partnerId", namespace = "http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", type = JAXBElement.class, required = false)
    protected JAXBElement<String> partnerId;
    @XmlElementRef(name = "persistentPromoCode", namespace = "http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", type = JAXBElement.class, required = false)
    protected JAXBElement<String> persistentPromoCode;
    @XmlElementRef(name = "promotionalCodeFlag", namespace = "http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> promotionalCodeFlag;
    @XmlElementRef(name = "shippingCostTotal", namespace = "http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", type = JAXBElement.class, required = false)
    protected JAXBElement<BigDecimal> shippingCostTotal;
    @XmlElementRef(name = "singleInstance", namespace = "http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", type = JAXBElement.class, required = false)
    protected JAXBElement<String> singleInstance;
    @XmlElementRef(name = "toNumberConsumer", namespace = "http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> toNumberConsumer;
    @XmlElement(required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar validFrom;
    @XmlElement(required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar validTo;
    @XmlElementRef(name = "vodafoneAuthomaticBilling", namespace = "http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> vodafoneAuthomaticBilling;
    @XmlElement(required = true)
    protected String zone;
    @XmlElementRef(name = "mPaymentId", namespace = "http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", type = JAXBElement.class, required = false)
    protected JAXBElement<String> mPaymentId;

    /**
     * Recupera il valore della proprietÓ arrayOfPaymentMethod.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfPaymentMethod }
     *     
     */
    public ArrayOfPaymentMethod getArrayOfPaymentMethod() {
        return arrayOfPaymentMethod;
    }

    /**
     * Imposta il valore della proprietÓ arrayOfPaymentMethod.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfPaymentMethod }
     *     
     */
    public void setArrayOfPaymentMethod(ArrayOfPaymentMethod value) {
        this.arrayOfPaymentMethod = value;
    }

    /**
     * Recupera il valore della proprietÓ arrayOfProduct.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfProduct }
     *     
     */
    public ArrayOfProduct getArrayOfProduct() {
        return arrayOfProduct;
    }

    /**
     * Imposta il valore della proprietÓ arrayOfProduct.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfProduct }
     *     
     */
    public void setArrayOfProduct(ArrayOfProduct value) {
        this.arrayOfProduct = value;
    }

    /**
     * Recupera il valore della proprietÓ arrayOfTipoAbbonamento.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfTipoAbbonamento }{@code >}
     *     
     */
    public JAXBElement<ArrayOfTipoAbbonamento> getArrayOfTipoAbbonamento() {
        return arrayOfTipoAbbonamento;
    }

    /**
     * Imposta il valore della proprietÓ arrayOfTipoAbbonamento.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfTipoAbbonamento }{@code >}
     *     
     */
    public void setArrayOfTipoAbbonamento(JAXBElement<ArrayOfTipoAbbonamento> value) {
        this.arrayOfTipoAbbonamento = value;
    }

    /**
     * Recupera il valore della proprietÓ businessUnit.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getBusinessUnit() {
        return businessUnit;
    }

    /**
     * Imposta il valore della proprietÓ businessUnit.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setBusinessUnit(JAXBElement<String> value) {
        this.businessUnit = value;
    }

    /**
     * Recupera il valore della proprietÓ customerSegmentation.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCustomerSegmentation() {
        return customerSegmentation;
    }

    /**
     * Imposta il valore della proprietÓ customerSegmentation.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCustomerSegmentation(String value) {
        this.customerSegmentation = value;
    }

    /**
     * Recupera il valore della proprietÓ discountPercentage.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getDiscountPercentage() {
        return discountPercentage;
    }

    /**
     * Imposta il valore della proprietÓ discountPercentage.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setDiscountPercentage(BigDecimal value) {
        this.discountPercentage = value;
    }

    /**
     * Recupera il valore della proprietÓ discountPercentageFinal.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getDiscountPercentageFinal() {
        return discountPercentageFinal;
    }

    /**
     * Imposta il valore della proprietÓ discountPercentageFinal.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setDiscountPercentageFinal(BigDecimal value) {
        this.discountPercentageFinal = value;
    }

    /**
     * Recupera il valore della proprietÓ discountValueFinal.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getDiscountValueFinal() {
        return discountValueFinal;
    }

    /**
     * Imposta il valore della proprietÓ discountValueFinal.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setDiscountValueFinal(BigDecimal value) {
        this.discountValueFinal = value;
    }

    /**
     * Recupera il valore della proprietÓ displayInfo.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link DisplayInfo }{@code >}
     *     
     */
    public JAXBElement<DisplayInfo> getDisplayInfo() {
        return displayInfo;
    }

    /**
     * Imposta il valore della proprietÓ displayInfo.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link DisplayInfo }{@code >}
     *     
     */
    public void setDisplayInfo(JAXBElement<DisplayInfo> value) {
        this.displayInfo = value;
    }

    /**
     * Recupera il valore della proprietÓ fromNumberConsumer.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getFromNumberConsumer() {
        return fromNumberConsumer;
    }

    /**
     * Imposta il valore della proprietÓ fromNumberConsumer.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setFromNumberConsumer(JAXBElement<Integer> value) {
        this.fromNumberConsumer = value;
    }

    /**
     * Recupera il valore della proprietÓ giftCode.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getGiftCode() {
        return giftCode;
    }

    /**
     * Imposta il valore della proprietÓ giftCode.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setGiftCode(JAXBElement<String> value) {
        this.giftCode = value;
    }

    /**
     * Recupera il valore della proprietÓ giftDescription.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getGiftDescription() {
        return giftDescription;
    }

    /**
     * Imposta il valore della proprietÓ giftDescription.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setGiftDescription(JAXBElement<String> value) {
        this.giftDescription = value;
    }

    /**
     * Recupera il valore della proprietÓ giftFlag.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getGiftFlag() {
        return giftFlag;
    }

    /**
     * Imposta il valore della proprietÓ giftFlag.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setGiftFlag(JAXBElement<Boolean> value) {
        this.giftFlag = value;
    }

    /**
     * Recupera il valore della proprietÓ gratisFirstPurchase.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getGratisFirstPurchase() {
        return gratisFirstPurchase;
    }

    /**
     * Imposta il valore della proprietÓ gratisFirstPurchase.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setGratisFirstPurchase(JAXBElement<String> value) {
        this.gratisFirstPurchase = value;
    }

    /**
     * Recupera il valore della proprietÓ grossPriceOffer.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getGrossPriceOffer() {
        return grossPriceOffer;
    }

    /**
     * Imposta il valore della proprietÓ grossPriceOffer.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setGrossPriceOffer(BigDecimal value) {
        this.grossPriceOffer = value;
    }

    /**
     * Recupera il valore della proprietÓ isCumulable.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getIsCumulable() {
        return isCumulable;
    }

    /**
     * Imposta il valore della proprietÓ isCumulable.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setIsCumulable(JAXBElement<Boolean> value) {
        this.isCumulable = value;
    }

    /**
     * Recupera il valore della proprietÓ isExecutive.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getIsExecutive() {
        return isExecutive;
    }

    /**
     * Imposta il valore della proprietÓ isExecutive.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setIsExecutive(JAXBElement<Boolean> value) {
        this.isExecutive = value;
    }

    /**
     * Recupera il valore della proprietÓ limiteGratis.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getLimiteGratis() {
        return limiteGratis;
    }

    /**
     * Imposta il valore della proprietÓ limiteGratis.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setLimiteGratis(JAXBElement<String> value) {
        this.limiteGratis = value;
    }

    /**
     * Recupera il valore della proprietÓ nestedOffer.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getNestedOffer() {
        return nestedOffer;
    }

    /**
     * Imposta il valore della proprietÓ nestedOffer.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setNestedOffer(JAXBElement<String> value) {
        this.nestedOffer = value;
    }

    /**
     * Recupera il valore della proprietÓ nestedOfferVersion.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getNestedOfferVersion() {
        return nestedOfferVersion;
    }

    /**
     * Imposta il valore della proprietÓ nestedOfferVersion.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setNestedOfferVersion(JAXBElement<String> value) {
        this.nestedOfferVersion = value;
    }

    /**
     * Recupera il valore della proprietÓ netFinalOfferPrice.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getNetFinalOfferPrice() {
        return netFinalOfferPrice;
    }

    /**
     * Imposta il valore della proprietÓ netFinalOfferPrice.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setNetFinalOfferPrice(BigDecimal value) {
        this.netFinalOfferPrice = value;
    }

    /**
     * Recupera il valore della proprietÓ netFinalOfferPriceNoIVA.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getNetFinalOfferPriceNoIVA() {
        return netFinalOfferPriceNoIVA;
    }

    /**
     * Imposta il valore della proprietÓ netFinalOfferPriceNoIVA.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setNetFinalOfferPriceNoIVA(BigDecimal value) {
        this.netFinalOfferPriceNoIVA = value;
    }

    /**
     * Recupera il valore della proprietÓ netPriceOffer.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getNetPriceOffer() {
        return netPriceOffer;
    }

    /**
     * Imposta il valore della proprietÓ netPriceOffer.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setNetPriceOffer(BigDecimal value) {
        this.netPriceOffer = value;
    }

    /**
     * Recupera il valore della proprietÓ nomePartner.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getNomePartner() {
        return nomePartner;
    }

    /**
     * Imposta il valore della proprietÓ nomePartner.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setNomePartner(JAXBElement<String> value) {
        this.nomePartner = value;
    }

    /**
     * Recupera il valore della proprietÓ offerDescription.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOfferDescription() {
        return offerDescription;
    }

    /**
     * Imposta il valore della proprietÓ offerDescription.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOfferDescription(String value) {
        this.offerDescription = value;
    }

    /**
     * Recupera il valore della proprietÓ offerId.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOfferId() {
        return offerId;
    }

    /**
     * Imposta il valore della proprietÓ offerId.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOfferId(String value) {
        this.offerId = value;
    }

    /**
     * Recupera il valore della proprietÓ offerLanguage.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getOfferLanguage() {
        return offerLanguage;
    }

    /**
     * Imposta il valore della proprietÓ offerLanguage.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setOfferLanguage(JAXBElement<String> value) {
        this.offerLanguage = value;
    }

    /**
     * Recupera il valore della proprietÓ offerName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOfferName() {
        return offerName;
    }

    /**
     * Imposta il valore della proprietÓ offerName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOfferName(String value) {
        this.offerName = value;
    }

    /**
     * Recupera il valore della proprietÓ offerStatus.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getOfferStatus() {
        return offerStatus;
    }

    /**
     * Imposta il valore della proprietÓ offerStatus.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setOfferStatus(JAXBElement<String> value) {
        this.offerStatus = value;
    }

    /**
     * Recupera il valore della proprietÓ offerType.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOfferType() {
        return offerType;
    }

    /**
     * Imposta il valore della proprietÓ offerType.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOfferType(String value) {
        this.offerType = value;
    }

    /**
     * Recupera il valore della proprietÓ offerVersion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOfferVersion() {
        return offerVersion;
    }

    /**
     * Imposta il valore della proprietÓ offerVersion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOfferVersion(String value) {
        this.offerVersion = value;
    }

    /**
     * Recupera il valore della proprietÓ partnerId.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPartnerId() {
        return partnerId;
    }

    /**
     * Imposta il valore della proprietÓ partnerId.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPartnerId(JAXBElement<String> value) {
        this.partnerId = value;
    }

    /**
     * Recupera il valore della proprietÓ persistentPromoCode.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPersistentPromoCode() {
        return persistentPromoCode;
    }

    /**
     * Imposta il valore della proprietÓ persistentPromoCode.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPersistentPromoCode(JAXBElement<String> value) {
        this.persistentPromoCode = value;
    }

    /**
     * Recupera il valore della proprietÓ promotionalCodeFlag.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getPromotionalCodeFlag() {
        return promotionalCodeFlag;
    }

    /**
     * Imposta il valore della proprietÓ promotionalCodeFlag.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setPromotionalCodeFlag(JAXBElement<Boolean> value) {
        this.promotionalCodeFlag = value;
    }

    /**
     * Recupera il valore della proprietÓ shippingCostTotal.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}
     *     
     */
    public JAXBElement<BigDecimal> getShippingCostTotal() {
        return shippingCostTotal;
    }

    /**
     * Imposta il valore della proprietÓ shippingCostTotal.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}
     *     
     */
    public void setShippingCostTotal(JAXBElement<BigDecimal> value) {
        this.shippingCostTotal = value;
    }

    /**
     * Recupera il valore della proprietÓ singleInstance.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getSingleInstance() {
        return singleInstance;
    }

    /**
     * Imposta il valore della proprietÓ singleInstance.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setSingleInstance(JAXBElement<String> value) {
        this.singleInstance = value;
    }

    /**
     * Recupera il valore della proprietÓ toNumberConsumer.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getToNumberConsumer() {
        return toNumberConsumer;
    }

    /**
     * Imposta il valore della proprietÓ toNumberConsumer.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setToNumberConsumer(JAXBElement<Integer> value) {
        this.toNumberConsumer = value;
    }

    /**
     * Recupera il valore della proprietÓ validFrom.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getValidFrom() {
        return validFrom;
    }

    /**
     * Imposta il valore della proprietÓ validFrom.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setValidFrom(XMLGregorianCalendar value) {
        this.validFrom = value;
    }

    /**
     * Recupera il valore della proprietÓ validTo.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getValidTo() {
        return validTo;
    }

    /**
     * Imposta il valore della proprietÓ validTo.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setValidTo(XMLGregorianCalendar value) {
        this.validTo = value;
    }

    /**
     * Recupera il valore della proprietÓ vodafoneAuthomaticBilling.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getVodafoneAuthomaticBilling() {
        return vodafoneAuthomaticBilling;
    }

    /**
     * Imposta il valore della proprietÓ vodafoneAuthomaticBilling.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setVodafoneAuthomaticBilling(JAXBElement<Boolean> value) {
        this.vodafoneAuthomaticBilling = value;
    }

    /**
     * Recupera il valore della proprietÓ zone.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getZone() {
        return zone;
    }

    /**
     * Imposta il valore della proprietÓ zone.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setZone(String value) {
        this.zone = value;
    }

    /**
     * Recupera il valore della proprietÓ mPaymentId.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getMPaymentId() {
        return mPaymentId;
    }

    /**
     * Imposta il valore della proprietÓ mPaymentId.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setMPaymentId(JAXBElement<String> value) {
        this.mPaymentId = value;
    }

}


package it.rcs.enfinity.rcs_bc_soa.capi.soa.displayinfo.xsd;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per DisplayInfo complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="DisplayInfo"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="author" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="imgURL" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="longDesc" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="releaseDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="shortDesc" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="subtitle" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="thumbURL" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="title" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="title2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="videoURL" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DisplayInfo", propOrder = {
    "author",
    "imgURL",
    "longDesc",
    "releaseDate",
    "shortDesc",
    "subtitle",
    "thumbURL",
    "title",
    "title2",
    "videoURL"
})
public class DisplayInfo {

    @XmlElementRef(name = "author", namespace = "http://displayInfo.soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", type = JAXBElement.class, required = false)
    protected JAXBElement<String> author;
    @XmlElementRef(name = "imgURL", namespace = "http://displayInfo.soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", type = JAXBElement.class, required = false)
    protected JAXBElement<String> imgURL;
    @XmlElementRef(name = "longDesc", namespace = "http://displayInfo.soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", type = JAXBElement.class, required = false)
    protected JAXBElement<String> longDesc;
    @XmlElementRef(name = "releaseDate", namespace = "http://displayInfo.soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", type = JAXBElement.class, required = false)
    protected JAXBElement<String> releaseDate;
    @XmlElementRef(name = "shortDesc", namespace = "http://displayInfo.soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", type = JAXBElement.class, required = false)
    protected JAXBElement<String> shortDesc;
    @XmlElementRef(name = "subtitle", namespace = "http://displayInfo.soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", type = JAXBElement.class, required = false)
    protected JAXBElement<String> subtitle;
    @XmlElementRef(name = "thumbURL", namespace = "http://displayInfo.soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", type = JAXBElement.class, required = false)
    protected JAXBElement<String> thumbURL;
    @XmlElement(required = true)
    protected String title;
    @XmlElementRef(name = "title2", namespace = "http://displayInfo.soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", type = JAXBElement.class, required = false)
    protected JAXBElement<String> title2;
    @XmlElementRef(name = "videoURL", namespace = "http://displayInfo.soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", type = JAXBElement.class, required = false)
    protected JAXBElement<String> videoURL;

    /**
     * Recupera il valore della proprietÓ author.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getAuthor() {
        return author;
    }

    /**
     * Imposta il valore della proprietÓ author.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setAuthor(JAXBElement<String> value) {
        this.author = value;
    }

    /**
     * Recupera il valore della proprietÓ imgURL.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getImgURL() {
        return imgURL;
    }

    /**
     * Imposta il valore della proprietÓ imgURL.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setImgURL(JAXBElement<String> value) {
        this.imgURL = value;
    }

    /**
     * Recupera il valore della proprietÓ longDesc.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getLongDesc() {
        return longDesc;
    }

    /**
     * Imposta il valore della proprietÓ longDesc.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setLongDesc(JAXBElement<String> value) {
        this.longDesc = value;
    }

    /**
     * Recupera il valore della proprietÓ releaseDate.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getReleaseDate() {
        return releaseDate;
    }

    /**
     * Imposta il valore della proprietÓ releaseDate.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setReleaseDate(JAXBElement<String> value) {
        this.releaseDate = value;
    }

    /**
     * Recupera il valore della proprietÓ shortDesc.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getShortDesc() {
        return shortDesc;
    }

    /**
     * Imposta il valore della proprietÓ shortDesc.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setShortDesc(JAXBElement<String> value) {
        this.shortDesc = value;
    }

    /**
     * Recupera il valore della proprietÓ subtitle.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getSubtitle() {
        return subtitle;
    }

    /**
     * Imposta il valore della proprietÓ subtitle.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setSubtitle(JAXBElement<String> value) {
        this.subtitle = value;
    }

    /**
     * Recupera il valore della proprietÓ thumbURL.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getThumbURL() {
        return thumbURL;
    }

    /**
     * Imposta il valore della proprietÓ thumbURL.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setThumbURL(JAXBElement<String> value) {
        this.thumbURL = value;
    }

    /**
     * Recupera il valore della proprietÓ title.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTitle() {
        return title;
    }

    /**
     * Imposta il valore della proprietÓ title.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTitle(String value) {
        this.title = value;
    }

    /**
     * Recupera il valore della proprietÓ title2.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getTitle2() {
        return title2;
    }

    /**
     * Imposta il valore della proprietÓ title2.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setTitle2(JAXBElement<String> value) {
        this.title2 = value;
    }

    /**
     * Recupera il valore della proprietÓ videoURL.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getVideoURL() {
        return videoURL;
    }

    /**
     * Imposta il valore della proprietÓ videoURL.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setVideoURL(JAXBElement<String> value) {
        this.videoURL = value;
    }

}


package it.rcs.enfinity.rcs_bc_soa.capi.soa.xsd;

import java.math.BigDecimal;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import it.rcs.enfinity.rcs_bc_soa.capi.soa.editorialentity.xsd.EditorialEntity;


/**
 * <p>Classe Java per Product complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="Product"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="accessNumber" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/&gt;
 *         &lt;element name="adsCertificationCode" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="arrayOfPromotionChain" type="{http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd}ArrayOfPromotionChain" minOccurs="0"/&gt;
 *         &lt;element name="copiesNumberOnFriday" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/&gt;
 *         &lt;element name="copiesNumberOnMonday" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/&gt;
 *         &lt;element name="copiesNumberOnSaturday" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/&gt;
 *         &lt;element name="copiesNumberOnSunday" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/&gt;
 *         &lt;element name="copiesNumberOnThursday" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/&gt;
 *         &lt;element name="copiesNumberOnTuesday" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/&gt;
 *         &lt;element name="copiesNumberOnWednesday" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/&gt;
 *         &lt;element name="dataFruizioneFrom" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/&gt;
 *         &lt;element name="dataFruizioneTo" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/&gt;
 *         &lt;element name="description" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="digitalProduct" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="discountAmount" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/&gt;
 *         &lt;element name="discountPercent" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/&gt;
 *         &lt;element name="discountType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="editorialEntity" type="{http://editorialEntity.soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd}EditorialEntity" minOccurs="0"/&gt;
 *         &lt;element name="fridayPrice" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/&gt;
 *         &lt;element name="fridayUse" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="isTrial" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="issueNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="issueYear" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="mondayPrice" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/&gt;
 *         &lt;element name="mondayUse" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="offerId" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="paymentType" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="pricingScheme" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="productId" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="productInOfferInitialPrice" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
 *         &lt;element name="productInOfferPriceGross" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
 *         &lt;element name="productInOfferPriceNet" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
 *         &lt;element name="productInOfferWithIva" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
 *         &lt;element name="productInOfferWithoutIva" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
 *         &lt;element name="productOfferId" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="productOfferName" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="productOfferType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="productPeriodicity" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="productTipology" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="productVersion" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="quantity" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/&gt;
 *         &lt;element name="recurrentPriceDiscountType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="recurrentPriceGross" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/&gt;
 *         &lt;element name="recurrentPriceNet" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/&gt;
 *         &lt;element name="recurrentPricePercentDiscount" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/&gt;
 *         &lt;element name="recurringPeriodicity" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="renewal" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="saturdayPrice" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/&gt;
 *         &lt;element name="saturdayUse" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="scivoloType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="shippingCost" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/&gt;
 *         &lt;element name="subscriptionDurationCopies" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/&gt;
 *         &lt;element name="subscriptionDurationDays" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/&gt;
 *         &lt;element name="subscriptionDurationFormat" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="subscriptionDurationMonths" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/&gt;
 *         &lt;element name="subscriptionDurationWeek" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/&gt;
 *         &lt;element name="subscriptionRecurrencesNumber" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/&gt;
 *         &lt;element name="sundayPrice" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/&gt;
 *         &lt;element name="sundayUse" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="taxation" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="testata" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="thursdayPrice" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/&gt;
 *         &lt;element name="thursdayUse" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="tuesdayPrice" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/&gt;
 *         &lt;element name="tuesdayUse" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="useType" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="wednesdayPrice" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/&gt;
 *         &lt;element name="wednesdayUse" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Product", propOrder = {
    "accessNumber",
    "adsCertificationCode",
    "arrayOfPromotionChain",
    "copiesNumberOnFriday",
    "copiesNumberOnMonday",
    "copiesNumberOnSaturday",
    "copiesNumberOnSunday",
    "copiesNumberOnThursday",
    "copiesNumberOnTuesday",
    "copiesNumberOnWednesday",
    "dataFruizioneFrom",
    "dataFruizioneTo",
    "description",
    "digitalProduct",
    "discountAmount",
    "discountPercent",
    "discountType",
    "editorialEntity",
    "fridayPrice",
    "fridayUse",
    "isTrial",
    "issueNumber",
    "issueYear",
    "mondayPrice",
    "mondayUse",
    "offerId",
    "paymentType",
    "pricingScheme",
    "productId",
    "productInOfferInitialPrice",
    "productInOfferPriceGross",
    "productInOfferPriceNet",
    "productInOfferWithIva",
    "productInOfferWithoutIva",
    "productOfferId",
    "productOfferName",
    "productOfferType",
    "productPeriodicity",
    "productTipology",
    "productVersion",
    "quantity",
    "recurrentPriceDiscountType",
    "recurrentPriceGross",
    "recurrentPriceNet",
    "recurrentPricePercentDiscount",
    "recurringPeriodicity",
    "renewal",
    "saturdayPrice",
    "saturdayUse",
    "scivoloType",
    "shippingCost",
    "subscriptionDurationCopies",
    "subscriptionDurationDays",
    "subscriptionDurationFormat",
    "subscriptionDurationMonths",
    "subscriptionDurationWeek",
    "subscriptionRecurrencesNumber",
    "sundayPrice",
    "sundayUse",
    "taxation",
    "testata",
    "thursdayPrice",
    "thursdayUse",
    "tuesdayPrice",
    "tuesdayUse",
    "useType",
    "wednesdayPrice",
    "wednesdayUse"
})
public class Product {

    @XmlElementRef(name = "accessNumber", namespace = "http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> accessNumber;
    @XmlElement(required = true)
    protected String adsCertificationCode;
    @XmlElementRef(name = "arrayOfPromotionChain", namespace = "http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfPromotionChain> arrayOfPromotionChain;
    @XmlElementRef(name = "copiesNumberOnFriday", namespace = "http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> copiesNumberOnFriday;
    @XmlElementRef(name = "copiesNumberOnMonday", namespace = "http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> copiesNumberOnMonday;
    @XmlElementRef(name = "copiesNumberOnSaturday", namespace = "http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> copiesNumberOnSaturday;
    @XmlElementRef(name = "copiesNumberOnSunday", namespace = "http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> copiesNumberOnSunday;
    @XmlElementRef(name = "copiesNumberOnThursday", namespace = "http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> copiesNumberOnThursday;
    @XmlElementRef(name = "copiesNumberOnTuesday", namespace = "http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> copiesNumberOnTuesday;
    @XmlElementRef(name = "copiesNumberOnWednesday", namespace = "http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> copiesNumberOnWednesday;
    @XmlElementRef(name = "dataFruizioneFrom", namespace = "http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", type = JAXBElement.class, required = false)
    protected JAXBElement<XMLGregorianCalendar> dataFruizioneFrom;
    @XmlElementRef(name = "dataFruizioneTo", namespace = "http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", type = JAXBElement.class, required = false)
    protected JAXBElement<XMLGregorianCalendar> dataFruizioneTo;
    @XmlElement(required = true)
    protected String description;
    @XmlElementRef(name = "digitalProduct", namespace = "http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", type = JAXBElement.class, required = false)
    protected JAXBElement<String> digitalProduct;
    @XmlElementRef(name = "discountAmount", namespace = "http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", type = JAXBElement.class, required = false)
    protected JAXBElement<BigDecimal> discountAmount;
    @XmlElementRef(name = "discountPercent", namespace = "http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", type = JAXBElement.class, required = false)
    protected JAXBElement<BigDecimal> discountPercent;
    @XmlElementRef(name = "discountType", namespace = "http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", type = JAXBElement.class, required = false)
    protected JAXBElement<String> discountType;
    @XmlElementRef(name = "editorialEntity", namespace = "http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", type = JAXBElement.class, required = false)
    protected JAXBElement<EditorialEntity> editorialEntity;
    @XmlElementRef(name = "fridayPrice", namespace = "http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", type = JAXBElement.class, required = false)
    protected JAXBElement<BigDecimal> fridayPrice;
    @XmlElementRef(name = "fridayUse", namespace = "http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> fridayUse;
    @XmlElementRef(name = "isTrial", namespace = "http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> isTrial;
    @XmlElementRef(name = "issueNumber", namespace = "http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", type = JAXBElement.class, required = false)
    protected JAXBElement<String> issueNumber;
    @XmlElementRef(name = "issueYear", namespace = "http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", type = JAXBElement.class, required = false)
    protected JAXBElement<String> issueYear;
    @XmlElementRef(name = "mondayPrice", namespace = "http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", type = JAXBElement.class, required = false)
    protected JAXBElement<BigDecimal> mondayPrice;
    @XmlElementRef(name = "mondayUse", namespace = "http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> mondayUse;
    @XmlElement(required = true)
    protected String offerId;
    @XmlElement(required = true)
    protected String paymentType;
    @XmlElementRef(name = "pricingScheme", namespace = "http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", type = JAXBElement.class, required = false)
    protected JAXBElement<String> pricingScheme;
    @XmlElement(required = true)
    protected String productId;
    @XmlElement(required = true)
    protected BigDecimal productInOfferInitialPrice;
    @XmlElement(required = true)
    protected BigDecimal productInOfferPriceGross;
    @XmlElement(required = true)
    protected BigDecimal productInOfferPriceNet;
    @XmlElement(required = true)
    protected BigDecimal productInOfferWithIva;
    @XmlElement(required = true)
    protected BigDecimal productInOfferWithoutIva;
    @XmlElement(required = true)
    protected String productOfferId;
    @XmlElement(required = true)
    protected String productOfferName;
    @XmlElementRef(name = "productOfferType", namespace = "http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", type = JAXBElement.class, required = false)
    protected JAXBElement<String> productOfferType;
    @XmlElementRef(name = "productPeriodicity", namespace = "http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", type = JAXBElement.class, required = false)
    protected JAXBElement<String> productPeriodicity;
    @XmlElementRef(name = "productTipology", namespace = "http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", type = JAXBElement.class, required = false)
    protected JAXBElement<String> productTipology;
    @XmlElement(required = true)
    protected String productVersion;
    @XmlElementRef(name = "quantity", namespace = "http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> quantity;
    @XmlElementRef(name = "recurrentPriceDiscountType", namespace = "http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", type = JAXBElement.class, required = false)
    protected JAXBElement<String> recurrentPriceDiscountType;
    @XmlElementRef(name = "recurrentPriceGross", namespace = "http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", type = JAXBElement.class, required = false)
    protected JAXBElement<BigDecimal> recurrentPriceGross;
    @XmlElementRef(name = "recurrentPriceNet", namespace = "http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", type = JAXBElement.class, required = false)
    protected JAXBElement<BigDecimal> recurrentPriceNet;
    @XmlElementRef(name = "recurrentPricePercentDiscount", namespace = "http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", type = JAXBElement.class, required = false)
    protected JAXBElement<BigDecimal> recurrentPricePercentDiscount;
    @XmlElementRef(name = "recurringPeriodicity", namespace = "http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", type = JAXBElement.class, required = false)
    protected JAXBElement<String> recurringPeriodicity;
    @XmlElement(required = true)
    protected String renewal;
    @XmlElementRef(name = "saturdayPrice", namespace = "http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", type = JAXBElement.class, required = false)
    protected JAXBElement<BigDecimal> saturdayPrice;
    @XmlElementRef(name = "saturdayUse", namespace = "http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> saturdayUse;
    @XmlElementRef(name = "scivoloType", namespace = "http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", type = JAXBElement.class, required = false)
    protected JAXBElement<String> scivoloType;
    @XmlElementRef(name = "shippingCost", namespace = "http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", type = JAXBElement.class, required = false)
    protected JAXBElement<BigDecimal> shippingCost;
    @XmlElementRef(name = "subscriptionDurationCopies", namespace = "http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> subscriptionDurationCopies;
    @XmlElementRef(name = "subscriptionDurationDays", namespace = "http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> subscriptionDurationDays;
    @XmlElementRef(name = "subscriptionDurationFormat", namespace = "http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", type = JAXBElement.class, required = false)
    protected JAXBElement<String> subscriptionDurationFormat;
    @XmlElementRef(name = "subscriptionDurationMonths", namespace = "http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> subscriptionDurationMonths;
    @XmlElementRef(name = "subscriptionDurationWeek", namespace = "http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> subscriptionDurationWeek;
    @XmlElementRef(name = "subscriptionRecurrencesNumber", namespace = "http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> subscriptionRecurrencesNumber;
    @XmlElementRef(name = "sundayPrice", namespace = "http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", type = JAXBElement.class, required = false)
    protected JAXBElement<BigDecimal> sundayPrice;
    @XmlElementRef(name = "sundayUse", namespace = "http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> sundayUse;
    protected int taxation;
    @XmlElement(required = true)
    protected String testata;
    @XmlElementRef(name = "thursdayPrice", namespace = "http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", type = JAXBElement.class, required = false)
    protected JAXBElement<BigDecimal> thursdayPrice;
    @XmlElementRef(name = "thursdayUse", namespace = "http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> thursdayUse;
    @XmlElementRef(name = "tuesdayPrice", namespace = "http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", type = JAXBElement.class, required = false)
    protected JAXBElement<BigDecimal> tuesdayPrice;
    @XmlElementRef(name = "tuesdayUse", namespace = "http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> tuesdayUse;
    @XmlElement(required = true)
    protected String useType;
    @XmlElementRef(name = "wednesdayPrice", namespace = "http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", type = JAXBElement.class, required = false)
    protected JAXBElement<BigDecimal> wednesdayPrice;
    @XmlElementRef(name = "wednesdayUse", namespace = "http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> wednesdayUse;

    /**
     * Recupera il valore della proprietÓ accessNumber.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getAccessNumber() {
        return accessNumber;
    }

    /**
     * Imposta il valore della proprietÓ accessNumber.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setAccessNumber(JAXBElement<Integer> value) {
        this.accessNumber = value;
    }

    /**
     * Recupera il valore della proprietÓ adsCertificationCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAdsCertificationCode() {
        return adsCertificationCode;
    }

    /**
     * Imposta il valore della proprietÓ adsCertificationCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAdsCertificationCode(String value) {
        this.adsCertificationCode = value;
    }

    /**
     * Recupera il valore della proprietÓ arrayOfPromotionChain.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfPromotionChain }{@code >}
     *     
     */
    public JAXBElement<ArrayOfPromotionChain> getArrayOfPromotionChain() {
        return arrayOfPromotionChain;
    }

    /**
     * Imposta il valore della proprietÓ arrayOfPromotionChain.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfPromotionChain }{@code >}
     *     
     */
    public void setArrayOfPromotionChain(JAXBElement<ArrayOfPromotionChain> value) {
        this.arrayOfPromotionChain = value;
    }

    /**
     * Recupera il valore della proprietÓ copiesNumberOnFriday.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getCopiesNumberOnFriday() {
        return copiesNumberOnFriday;
    }

    /**
     * Imposta il valore della proprietÓ copiesNumberOnFriday.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setCopiesNumberOnFriday(JAXBElement<Integer> value) {
        this.copiesNumberOnFriday = value;
    }

    /**
     * Recupera il valore della proprietÓ copiesNumberOnMonday.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getCopiesNumberOnMonday() {
        return copiesNumberOnMonday;
    }

    /**
     * Imposta il valore della proprietÓ copiesNumberOnMonday.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setCopiesNumberOnMonday(JAXBElement<Integer> value) {
        this.copiesNumberOnMonday = value;
    }

    /**
     * Recupera il valore della proprietÓ copiesNumberOnSaturday.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getCopiesNumberOnSaturday() {
        return copiesNumberOnSaturday;
    }

    /**
     * Imposta il valore della proprietÓ copiesNumberOnSaturday.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setCopiesNumberOnSaturday(JAXBElement<Integer> value) {
        this.copiesNumberOnSaturday = value;
    }

    /**
     * Recupera il valore della proprietÓ copiesNumberOnSunday.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getCopiesNumberOnSunday() {
        return copiesNumberOnSunday;
    }

    /**
     * Imposta il valore della proprietÓ copiesNumberOnSunday.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setCopiesNumberOnSunday(JAXBElement<Integer> value) {
        this.copiesNumberOnSunday = value;
    }

    /**
     * Recupera il valore della proprietÓ copiesNumberOnThursday.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getCopiesNumberOnThursday() {
        return copiesNumberOnThursday;
    }

    /**
     * Imposta il valore della proprietÓ copiesNumberOnThursday.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setCopiesNumberOnThursday(JAXBElement<Integer> value) {
        this.copiesNumberOnThursday = value;
    }

    /**
     * Recupera il valore della proprietÓ copiesNumberOnTuesday.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getCopiesNumberOnTuesday() {
        return copiesNumberOnTuesday;
    }

    /**
     * Imposta il valore della proprietÓ copiesNumberOnTuesday.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setCopiesNumberOnTuesday(JAXBElement<Integer> value) {
        this.copiesNumberOnTuesday = value;
    }

    /**
     * Recupera il valore della proprietÓ copiesNumberOnWednesday.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getCopiesNumberOnWednesday() {
        return copiesNumberOnWednesday;
    }

    /**
     * Imposta il valore della proprietÓ copiesNumberOnWednesday.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setCopiesNumberOnWednesday(JAXBElement<Integer> value) {
        this.copiesNumberOnWednesday = value;
    }

    /**
     * Recupera il valore della proprietÓ dataFruizioneFrom.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public JAXBElement<XMLGregorianCalendar> getDataFruizioneFrom() {
        return dataFruizioneFrom;
    }

    /**
     * Imposta il valore della proprietÓ dataFruizioneFrom.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public void setDataFruizioneFrom(JAXBElement<XMLGregorianCalendar> value) {
        this.dataFruizioneFrom = value;
    }

    /**
     * Recupera il valore della proprietÓ dataFruizioneTo.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public JAXBElement<XMLGregorianCalendar> getDataFruizioneTo() {
        return dataFruizioneTo;
    }

    /**
     * Imposta il valore della proprietÓ dataFruizioneTo.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public void setDataFruizioneTo(JAXBElement<XMLGregorianCalendar> value) {
        this.dataFruizioneTo = value;
    }

    /**
     * Recupera il valore della proprietÓ description.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Imposta il valore della proprietÓ description.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Recupera il valore della proprietÓ digitalProduct.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDigitalProduct() {
        return digitalProduct;
    }

    /**
     * Imposta il valore della proprietÓ digitalProduct.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDigitalProduct(JAXBElement<String> value) {
        this.digitalProduct = value;
    }

    /**
     * Recupera il valore della proprietÓ discountAmount.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}
     *     
     */
    public JAXBElement<BigDecimal> getDiscountAmount() {
        return discountAmount;
    }

    /**
     * Imposta il valore della proprietÓ discountAmount.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}
     *     
     */
    public void setDiscountAmount(JAXBElement<BigDecimal> value) {
        this.discountAmount = value;
    }

    /**
     * Recupera il valore della proprietÓ discountPercent.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}
     *     
     */
    public JAXBElement<BigDecimal> getDiscountPercent() {
        return discountPercent;
    }

    /**
     * Imposta il valore della proprietÓ discountPercent.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}
     *     
     */
    public void setDiscountPercent(JAXBElement<BigDecimal> value) {
        this.discountPercent = value;
    }

    /**
     * Recupera il valore della proprietÓ discountType.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDiscountType() {
        return discountType;
    }

    /**
     * Imposta il valore della proprietÓ discountType.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDiscountType(JAXBElement<String> value) {
        this.discountType = value;
    }

    /**
     * Recupera il valore della proprietÓ editorialEntity.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link EditorialEntity }{@code >}
     *     
     */
    public JAXBElement<EditorialEntity> getEditorialEntity() {
        return editorialEntity;
    }

    /**
     * Imposta il valore della proprietÓ editorialEntity.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link EditorialEntity }{@code >}
     *     
     */
    public void setEditorialEntity(JAXBElement<EditorialEntity> value) {
        this.editorialEntity = value;
    }

    /**
     * Recupera il valore della proprietÓ fridayPrice.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}
     *     
     */
    public JAXBElement<BigDecimal> getFridayPrice() {
        return fridayPrice;
    }

    /**
     * Imposta il valore della proprietÓ fridayPrice.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}
     *     
     */
    public void setFridayPrice(JAXBElement<BigDecimal> value) {
        this.fridayPrice = value;
    }

    /**
     * Recupera il valore della proprietÓ fridayUse.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getFridayUse() {
        return fridayUse;
    }

    /**
     * Imposta il valore della proprietÓ fridayUse.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setFridayUse(JAXBElement<Boolean> value) {
        this.fridayUse = value;
    }

    /**
     * Recupera il valore della proprietÓ isTrial.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getIsTrial() {
        return isTrial;
    }

    /**
     * Imposta il valore della proprietÓ isTrial.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setIsTrial(JAXBElement<Boolean> value) {
        this.isTrial = value;
    }

    /**
     * Recupera il valore della proprietÓ issueNumber.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getIssueNumber() {
        return issueNumber;
    }

    /**
     * Imposta il valore della proprietÓ issueNumber.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setIssueNumber(JAXBElement<String> value) {
        this.issueNumber = value;
    }

    /**
     * Recupera il valore della proprietÓ issueYear.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getIssueYear() {
        return issueYear;
    }

    /**
     * Imposta il valore della proprietÓ issueYear.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setIssueYear(JAXBElement<String> value) {
        this.issueYear = value;
    }

    /**
     * Recupera il valore della proprietÓ mondayPrice.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}
     *     
     */
    public JAXBElement<BigDecimal> getMondayPrice() {
        return mondayPrice;
    }

    /**
     * Imposta il valore della proprietÓ mondayPrice.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}
     *     
     */
    public void setMondayPrice(JAXBElement<BigDecimal> value) {
        this.mondayPrice = value;
    }

    /**
     * Recupera il valore della proprietÓ mondayUse.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getMondayUse() {
        return mondayUse;
    }

    /**
     * Imposta il valore della proprietÓ mondayUse.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setMondayUse(JAXBElement<Boolean> value) {
        this.mondayUse = value;
    }

    /**
     * Recupera il valore della proprietÓ offerId.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOfferId() {
        return offerId;
    }

    /**
     * Imposta il valore della proprietÓ offerId.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOfferId(String value) {
        this.offerId = value;
    }

    /**
     * Recupera il valore della proprietÓ paymentType.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPaymentType() {
        return paymentType;
    }

    /**
     * Imposta il valore della proprietÓ paymentType.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPaymentType(String value) {
        this.paymentType = value;
    }

    /**
     * Recupera il valore della proprietÓ pricingScheme.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPricingScheme() {
        return pricingScheme;
    }

    /**
     * Imposta il valore della proprietÓ pricingScheme.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPricingScheme(JAXBElement<String> value) {
        this.pricingScheme = value;
    }

    /**
     * Recupera il valore della proprietÓ productId.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProductId() {
        return productId;
    }

    /**
     * Imposta il valore della proprietÓ productId.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProductId(String value) {
        this.productId = value;
    }

    /**
     * Recupera il valore della proprietÓ productInOfferInitialPrice.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getProductInOfferInitialPrice() {
        return productInOfferInitialPrice;
    }

    /**
     * Imposta il valore della proprietÓ productInOfferInitialPrice.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setProductInOfferInitialPrice(BigDecimal value) {
        this.productInOfferInitialPrice = value;
    }

    /**
     * Recupera il valore della proprietÓ productInOfferPriceGross.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getProductInOfferPriceGross() {
        return productInOfferPriceGross;
    }

    /**
     * Imposta il valore della proprietÓ productInOfferPriceGross.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setProductInOfferPriceGross(BigDecimal value) {
        this.productInOfferPriceGross = value;
    }

    /**
     * Recupera il valore della proprietÓ productInOfferPriceNet.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getProductInOfferPriceNet() {
        return productInOfferPriceNet;
    }

    /**
     * Imposta il valore della proprietÓ productInOfferPriceNet.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setProductInOfferPriceNet(BigDecimal value) {
        this.productInOfferPriceNet = value;
    }

    /**
     * Recupera il valore della proprietÓ productInOfferWithIva.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getProductInOfferWithIva() {
        return productInOfferWithIva;
    }

    /**
     * Imposta il valore della proprietÓ productInOfferWithIva.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setProductInOfferWithIva(BigDecimal value) {
        this.productInOfferWithIva = value;
    }

    /**
     * Recupera il valore della proprietÓ productInOfferWithoutIva.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getProductInOfferWithoutIva() {
        return productInOfferWithoutIva;
    }

    /**
     * Imposta il valore della proprietÓ productInOfferWithoutIva.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setProductInOfferWithoutIva(BigDecimal value) {
        this.productInOfferWithoutIva = value;
    }

    /**
     * Recupera il valore della proprietÓ productOfferId.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProductOfferId() {
        return productOfferId;
    }

    /**
     * Imposta il valore della proprietÓ productOfferId.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProductOfferId(String value) {
        this.productOfferId = value;
    }

    /**
     * Recupera il valore della proprietÓ productOfferName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProductOfferName() {
        return productOfferName;
    }

    /**
     * Imposta il valore della proprietÓ productOfferName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProductOfferName(String value) {
        this.productOfferName = value;
    }

    /**
     * Recupera il valore della proprietÓ productOfferType.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getProductOfferType() {
        return productOfferType;
    }

    /**
     * Imposta il valore della proprietÓ productOfferType.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setProductOfferType(JAXBElement<String> value) {
        this.productOfferType = value;
    }

    /**
     * Recupera il valore della proprietÓ productPeriodicity.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getProductPeriodicity() {
        return productPeriodicity;
    }

    /**
     * Imposta il valore della proprietÓ productPeriodicity.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setProductPeriodicity(JAXBElement<String> value) {
        this.productPeriodicity = value;
    }

    /**
     * Recupera il valore della proprietÓ productTipology.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getProductTipology() {
        return productTipology;
    }

    /**
     * Imposta il valore della proprietÓ productTipology.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setProductTipology(JAXBElement<String> value) {
        this.productTipology = value;
    }

    /**
     * Recupera il valore della proprietÓ productVersion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProductVersion() {
        return productVersion;
    }

    /**
     * Imposta il valore della proprietÓ productVersion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProductVersion(String value) {
        this.productVersion = value;
    }

    /**
     * Recupera il valore della proprietÓ quantity.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getQuantity() {
        return quantity;
    }

    /**
     * Imposta il valore della proprietÓ quantity.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setQuantity(JAXBElement<Integer> value) {
        this.quantity = value;
    }

    /**
     * Recupera il valore della proprietÓ recurrentPriceDiscountType.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getRecurrentPriceDiscountType() {
        return recurrentPriceDiscountType;
    }

    /**
     * Imposta il valore della proprietÓ recurrentPriceDiscountType.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setRecurrentPriceDiscountType(JAXBElement<String> value) {
        this.recurrentPriceDiscountType = value;
    }

    /**
     * Recupera il valore della proprietÓ recurrentPriceGross.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}
     *     
     */
    public JAXBElement<BigDecimal> getRecurrentPriceGross() {
        return recurrentPriceGross;
    }

    /**
     * Imposta il valore della proprietÓ recurrentPriceGross.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}
     *     
     */
    public void setRecurrentPriceGross(JAXBElement<BigDecimal> value) {
        this.recurrentPriceGross = value;
    }

    /**
     * Recupera il valore della proprietÓ recurrentPriceNet.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}
     *     
     */
    public JAXBElement<BigDecimal> getRecurrentPriceNet() {
        return recurrentPriceNet;
    }

    /**
     * Imposta il valore della proprietÓ recurrentPriceNet.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}
     *     
     */
    public void setRecurrentPriceNet(JAXBElement<BigDecimal> value) {
        this.recurrentPriceNet = value;
    }

    /**
     * Recupera il valore della proprietÓ recurrentPricePercentDiscount.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}
     *     
     */
    public JAXBElement<BigDecimal> getRecurrentPricePercentDiscount() {
        return recurrentPricePercentDiscount;
    }

    /**
     * Imposta il valore della proprietÓ recurrentPricePercentDiscount.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}
     *     
     */
    public void setRecurrentPricePercentDiscount(JAXBElement<BigDecimal> value) {
        this.recurrentPricePercentDiscount = value;
    }

    /**
     * Recupera il valore della proprietÓ recurringPeriodicity.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getRecurringPeriodicity() {
        return recurringPeriodicity;
    }

    /**
     * Imposta il valore della proprietÓ recurringPeriodicity.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setRecurringPeriodicity(JAXBElement<String> value) {
        this.recurringPeriodicity = value;
    }

    /**
     * Recupera il valore della proprietÓ renewal.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRenewal() {
        return renewal;
    }

    /**
     * Imposta il valore della proprietÓ renewal.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRenewal(String value) {
        this.renewal = value;
    }

    /**
     * Recupera il valore della proprietÓ saturdayPrice.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}
     *     
     */
    public JAXBElement<BigDecimal> getSaturdayPrice() {
        return saturdayPrice;
    }

    /**
     * Imposta il valore della proprietÓ saturdayPrice.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}
     *     
     */
    public void setSaturdayPrice(JAXBElement<BigDecimal> value) {
        this.saturdayPrice = value;
    }

    /**
     * Recupera il valore della proprietÓ saturdayUse.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getSaturdayUse() {
        return saturdayUse;
    }

    /**
     * Imposta il valore della proprietÓ saturdayUse.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setSaturdayUse(JAXBElement<Boolean> value) {
        this.saturdayUse = value;
    }

    /**
     * Recupera il valore della proprietÓ scivoloType.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getScivoloType() {
        return scivoloType;
    }

    /**
     * Imposta il valore della proprietÓ scivoloType.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setScivoloType(JAXBElement<String> value) {
        this.scivoloType = value;
    }

    /**
     * Recupera il valore della proprietÓ shippingCost.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}
     *     
     */
    public JAXBElement<BigDecimal> getShippingCost() {
        return shippingCost;
    }

    /**
     * Imposta il valore della proprietÓ shippingCost.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}
     *     
     */
    public void setShippingCost(JAXBElement<BigDecimal> value) {
        this.shippingCost = value;
    }

    /**
     * Recupera il valore della proprietÓ subscriptionDurationCopies.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getSubscriptionDurationCopies() {
        return subscriptionDurationCopies;
    }

    /**
     * Imposta il valore della proprietÓ subscriptionDurationCopies.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setSubscriptionDurationCopies(JAXBElement<Integer> value) {
        this.subscriptionDurationCopies = value;
    }

    /**
     * Recupera il valore della proprietÓ subscriptionDurationDays.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getSubscriptionDurationDays() {
        return subscriptionDurationDays;
    }

    /**
     * Imposta il valore della proprietÓ subscriptionDurationDays.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setSubscriptionDurationDays(JAXBElement<Integer> value) {
        this.subscriptionDurationDays = value;
    }

    /**
     * Recupera il valore della proprietÓ subscriptionDurationFormat.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getSubscriptionDurationFormat() {
        return subscriptionDurationFormat;
    }

    /**
     * Imposta il valore della proprietÓ subscriptionDurationFormat.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setSubscriptionDurationFormat(JAXBElement<String> value) {
        this.subscriptionDurationFormat = value;
    }

    /**
     * Recupera il valore della proprietÓ subscriptionDurationMonths.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getSubscriptionDurationMonths() {
        return subscriptionDurationMonths;
    }

    /**
     * Imposta il valore della proprietÓ subscriptionDurationMonths.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setSubscriptionDurationMonths(JAXBElement<Integer> value) {
        this.subscriptionDurationMonths = value;
    }

    /**
     * Recupera il valore della proprietÓ subscriptionDurationWeek.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getSubscriptionDurationWeek() {
        return subscriptionDurationWeek;
    }

    /**
     * Imposta il valore della proprietÓ subscriptionDurationWeek.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setSubscriptionDurationWeek(JAXBElement<Integer> value) {
        this.subscriptionDurationWeek = value;
    }

    /**
     * Recupera il valore della proprietÓ subscriptionRecurrencesNumber.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getSubscriptionRecurrencesNumber() {
        return subscriptionRecurrencesNumber;
    }

    /**
     * Imposta il valore della proprietÓ subscriptionRecurrencesNumber.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setSubscriptionRecurrencesNumber(JAXBElement<Integer> value) {
        this.subscriptionRecurrencesNumber = value;
    }

    /**
     * Recupera il valore della proprietÓ sundayPrice.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}
     *     
     */
    public JAXBElement<BigDecimal> getSundayPrice() {
        return sundayPrice;
    }

    /**
     * Imposta il valore della proprietÓ sundayPrice.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}
     *     
     */
    public void setSundayPrice(JAXBElement<BigDecimal> value) {
        this.sundayPrice = value;
    }

    /**
     * Recupera il valore della proprietÓ sundayUse.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getSundayUse() {
        return sundayUse;
    }

    /**
     * Imposta il valore della proprietÓ sundayUse.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setSundayUse(JAXBElement<Boolean> value) {
        this.sundayUse = value;
    }

    /**
     * Recupera il valore della proprietÓ taxation.
     * 
     */
    public int getTaxation() {
        return taxation;
    }

    /**
     * Imposta il valore della proprietÓ taxation.
     * 
     */
    public void setTaxation(int value) {
        this.taxation = value;
    }

    /**
     * Recupera il valore della proprietÓ testata.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTestata() {
        return testata;
    }

    /**
     * Imposta il valore della proprietÓ testata.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTestata(String value) {
        this.testata = value;
    }

    /**
     * Recupera il valore della proprietÓ thursdayPrice.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}
     *     
     */
    public JAXBElement<BigDecimal> getThursdayPrice() {
        return thursdayPrice;
    }

    /**
     * Imposta il valore della proprietÓ thursdayPrice.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}
     *     
     */
    public void setThursdayPrice(JAXBElement<BigDecimal> value) {
        this.thursdayPrice = value;
    }

    /**
     * Recupera il valore della proprietÓ thursdayUse.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getThursdayUse() {
        return thursdayUse;
    }

    /**
     * Imposta il valore della proprietÓ thursdayUse.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setThursdayUse(JAXBElement<Boolean> value) {
        this.thursdayUse = value;
    }

    /**
     * Recupera il valore della proprietÓ tuesdayPrice.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}
     *     
     */
    public JAXBElement<BigDecimal> getTuesdayPrice() {
        return tuesdayPrice;
    }

    /**
     * Imposta il valore della proprietÓ tuesdayPrice.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}
     *     
     */
    public void setTuesdayPrice(JAXBElement<BigDecimal> value) {
        this.tuesdayPrice = value;
    }

    /**
     * Recupera il valore della proprietÓ tuesdayUse.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getTuesdayUse() {
        return tuesdayUse;
    }

    /**
     * Imposta il valore della proprietÓ tuesdayUse.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setTuesdayUse(JAXBElement<Boolean> value) {
        this.tuesdayUse = value;
    }

    /**
     * Recupera il valore della proprietÓ useType.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUseType() {
        return useType;
    }

    /**
     * Imposta il valore della proprietÓ useType.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUseType(String value) {
        this.useType = value;
    }

    /**
     * Recupera il valore della proprietÓ wednesdayPrice.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}
     *     
     */
    public JAXBElement<BigDecimal> getWednesdayPrice() {
        return wednesdayPrice;
    }

    /**
     * Imposta il valore della proprietÓ wednesdayPrice.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}
     *     
     */
    public void setWednesdayPrice(JAXBElement<BigDecimal> value) {
        this.wednesdayPrice = value;
    }

    /**
     * Recupera il valore della proprietÓ wednesdayUse.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getWednesdayUse() {
        return wednesdayUse;
    }

    /**
     * Imposta il valore della proprietÓ wednesdayUse.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setWednesdayUse(JAXBElement<Boolean> value) {
        this.wednesdayUse = value;
    }

}

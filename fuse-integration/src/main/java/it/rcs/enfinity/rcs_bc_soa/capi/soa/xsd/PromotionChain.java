
package it.rcs.enfinity.rcs_bc_soa.capi.soa.xsd;

import java.math.BigDecimal;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per PromotionChain complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="PromotionChain"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="intervalRecurrenceNumber" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/&gt;
 *         &lt;element name="scivoloId" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="scivoloIntervalDuration" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="scivoloIntervalRecurrencyNetPrice" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
 *         &lt;element name="scivoloNoDisable" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="scivoloRateale" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PromotionChain", propOrder = {
    "intervalRecurrenceNumber",
    "scivoloId",
    "scivoloIntervalDuration",
    "scivoloIntervalRecurrencyNetPrice",
    "scivoloNoDisable",
    "scivoloRateale"
})
public class PromotionChain {

    @XmlElementRef(name = "intervalRecurrenceNumber", namespace = "http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> intervalRecurrenceNumber;
    protected int scivoloId;
    protected int scivoloIntervalDuration;
    @XmlElement(required = true)
    protected BigDecimal scivoloIntervalRecurrencyNetPrice;
    @XmlElementRef(name = "scivoloNoDisable", namespace = "http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> scivoloNoDisable;
    @XmlElementRef(name = "scivoloRateale", namespace = "http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> scivoloRateale;

    /**
     * Recupera il valore della proprietÓ intervalRecurrenceNumber.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getIntervalRecurrenceNumber() {
        return intervalRecurrenceNumber;
    }

    /**
     * Imposta il valore della proprietÓ intervalRecurrenceNumber.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setIntervalRecurrenceNumber(JAXBElement<Integer> value) {
        this.intervalRecurrenceNumber = value;
    }

    /**
     * Recupera il valore della proprietÓ scivoloId.
     * 
     */
    public int getScivoloId() {
        return scivoloId;
    }

    /**
     * Imposta il valore della proprietÓ scivoloId.
     * 
     */
    public void setScivoloId(int value) {
        this.scivoloId = value;
    }

    /**
     * Recupera il valore della proprietÓ scivoloIntervalDuration.
     * 
     */
    public int getScivoloIntervalDuration() {
        return scivoloIntervalDuration;
    }

    /**
     * Imposta il valore della proprietÓ scivoloIntervalDuration.
     * 
     */
    public void setScivoloIntervalDuration(int value) {
        this.scivoloIntervalDuration = value;
    }

    /**
     * Recupera il valore della proprietÓ scivoloIntervalRecurrencyNetPrice.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getScivoloIntervalRecurrencyNetPrice() {
        return scivoloIntervalRecurrencyNetPrice;
    }

    /**
     * Imposta il valore della proprietÓ scivoloIntervalRecurrencyNetPrice.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setScivoloIntervalRecurrencyNetPrice(BigDecimal value) {
        this.scivoloIntervalRecurrencyNetPrice = value;
    }

    /**
     * Recupera il valore della proprietÓ scivoloNoDisable.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getScivoloNoDisable() {
        return scivoloNoDisable;
    }

    /**
     * Imposta il valore della proprietÓ scivoloNoDisable.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setScivoloNoDisable(JAXBElement<Boolean> value) {
        this.scivoloNoDisable = value;
    }

    /**
     * Recupera il valore della proprietÓ scivoloRateale.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getScivoloRateale() {
        return scivoloRateale;
    }

    /**
     * Imposta il valore della proprietÓ scivoloRateale.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setScivoloRateale(JAXBElement<Boolean> value) {
        this.scivoloRateale = value;
    }

}

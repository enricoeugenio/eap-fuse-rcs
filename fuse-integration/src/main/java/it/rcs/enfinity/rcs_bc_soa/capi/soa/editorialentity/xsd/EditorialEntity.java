
package it.rcs.enfinity.rcs_bc_soa.capi.soa.editorialentity.xsd;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per EditorialEntity complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="EditorialEntity"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="contextID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="editorialEntityID" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="membershipPaywall" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="name" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EditorialEntity", propOrder = {
    "contextID",
    "editorialEntityID",
    "membershipPaywall",
    "name"
})
public class EditorialEntity {

    @XmlElementRef(name = "contextID", namespace = "http://editorialEntity.soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", type = JAXBElement.class, required = false)
    protected JAXBElement<String> contextID;
    @XmlElement(required = true)
    protected String editorialEntityID;
    @XmlElementRef(name = "membershipPaywall", namespace = "http://editorialEntity.soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> membershipPaywall;
    @XmlElementRef(name = "name", namespace = "http://editorialEntity.soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", type = JAXBElement.class, required = false)
    protected JAXBElement<String> name;

    /**
     * Recupera il valore della proprietÓ contextID.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getContextID() {
        return contextID;
    }

    /**
     * Imposta il valore della proprietÓ contextID.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setContextID(JAXBElement<String> value) {
        this.contextID = value;
    }

    /**
     * Recupera il valore della proprietÓ editorialEntityID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEditorialEntityID() {
        return editorialEntityID;
    }

    /**
     * Imposta il valore della proprietÓ editorialEntityID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEditorialEntityID(String value) {
        this.editorialEntityID = value;
    }

    /**
     * Recupera il valore della proprietÓ membershipPaywall.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getMembershipPaywall() {
        return membershipPaywall;
    }

    /**
     * Imposta il valore della proprietÓ membershipPaywall.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setMembershipPaywall(JAXBElement<Boolean> value) {
        this.membershipPaywall = value;
    }

    /**
     * Recupera il valore della proprietÓ name.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getName() {
        return name;
    }

    /**
     * Imposta il valore della proprietÓ name.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setName(JAXBElement<String> value) {
        this.name = value;
    }

}


package it.rcs.enfinity.rcs_bc_soa.capi.soa.xsd;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per ArrayOfTipoAbbonamento complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfTipoAbbonamento"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="tipoAbbonamento" type="{http://soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd}TipoAbbonamento" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfTipoAbbonamento", propOrder = {
    "tipoAbbonamento"
})
public class ArrayOfTipoAbbonamento {

    @XmlElement(nillable = true)
    protected List<TipoAbbonamento> tipoAbbonamento;

    /**
     * Gets the value of the tipoAbbonamento property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the tipoAbbonamento property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTipoAbbonamento().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TipoAbbonamento }
     * 
     * 
     */
    public List<TipoAbbonamento> getTipoAbbonamento() {
        if (tipoAbbonamento == null) {
            tipoAbbonamento = new ArrayList<TipoAbbonamento>();
        }
        return this.tipoAbbonamento;
    }

}


package it.rcs.enfinity.rcs_bc_soa.capi.soa.displayinfo.xsd;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the it.rcs.enfinity.rcs_bc_soa.capi.soa.displayinfo.xsd package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _DisplayInfoAuthor_QNAME = new QName("http://displayInfo.soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", "author");
    private final static QName _DisplayInfoImgURL_QNAME = new QName("http://displayInfo.soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", "imgURL");
    private final static QName _DisplayInfoLongDesc_QNAME = new QName("http://displayInfo.soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", "longDesc");
    private final static QName _DisplayInfoReleaseDate_QNAME = new QName("http://displayInfo.soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", "releaseDate");
    private final static QName _DisplayInfoShortDesc_QNAME = new QName("http://displayInfo.soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", "shortDesc");
    private final static QName _DisplayInfoSubtitle_QNAME = new QName("http://displayInfo.soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", "subtitle");
    private final static QName _DisplayInfoThumbURL_QNAME = new QName("http://displayInfo.soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", "thumbURL");
    private final static QName _DisplayInfoTitle2_QNAME = new QName("http://displayInfo.soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", "title2");
    private final static QName _DisplayInfoVideoURL_QNAME = new QName("http://displayInfo.soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", "videoURL");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: it.rcs.enfinity.rcs_bc_soa.capi.soa.displayinfo.xsd
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link DisplayInfo }
     * 
     */
    public DisplayInfo createDisplayInfo() {
        return new DisplayInfo();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://displayInfo.soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", name = "author", scope = DisplayInfo.class)
    public JAXBElement<String> createDisplayInfoAuthor(String value) {
        return new JAXBElement<String>(_DisplayInfoAuthor_QNAME, String.class, DisplayInfo.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://displayInfo.soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", name = "imgURL", scope = DisplayInfo.class)
    public JAXBElement<String> createDisplayInfoImgURL(String value) {
        return new JAXBElement<String>(_DisplayInfoImgURL_QNAME, String.class, DisplayInfo.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://displayInfo.soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", name = "longDesc", scope = DisplayInfo.class)
    public JAXBElement<String> createDisplayInfoLongDesc(String value) {
        return new JAXBElement<String>(_DisplayInfoLongDesc_QNAME, String.class, DisplayInfo.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://displayInfo.soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", name = "releaseDate", scope = DisplayInfo.class)
    public JAXBElement<String> createDisplayInfoReleaseDate(String value) {
        return new JAXBElement<String>(_DisplayInfoReleaseDate_QNAME, String.class, DisplayInfo.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://displayInfo.soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", name = "shortDesc", scope = DisplayInfo.class)
    public JAXBElement<String> createDisplayInfoShortDesc(String value) {
        return new JAXBElement<String>(_DisplayInfoShortDesc_QNAME, String.class, DisplayInfo.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://displayInfo.soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", name = "subtitle", scope = DisplayInfo.class)
    public JAXBElement<String> createDisplayInfoSubtitle(String value) {
        return new JAXBElement<String>(_DisplayInfoSubtitle_QNAME, String.class, DisplayInfo.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://displayInfo.soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", name = "thumbURL", scope = DisplayInfo.class)
    public JAXBElement<String> createDisplayInfoThumbURL(String value) {
        return new JAXBElement<String>(_DisplayInfoThumbURL_QNAME, String.class, DisplayInfo.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://displayInfo.soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", name = "title2", scope = DisplayInfo.class)
    public JAXBElement<String> createDisplayInfoTitle2(String value) {
        return new JAXBElement<String>(_DisplayInfoTitle2_QNAME, String.class, DisplayInfo.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://displayInfo.soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", name = "videoURL", scope = DisplayInfo.class)
    public JAXBElement<String> createDisplayInfoVideoURL(String value) {
        return new JAXBElement<String>(_DisplayInfoVideoURL_QNAME, String.class, DisplayInfo.class, value);
    }

}

package it.rcs.soa.fuse_integration.route;

import org.apache.camel.model.DataFormatDefinition;

import it.rcs.fuse.core.cxf.configuration.PolicyFifteenSecondsTimeoutEnpointConfigurer;
import it.rcs.fuse.core.cxf.configuration.PolicyFiveSecondsTimeoutEnpointConfigurer;
import it.rcs.fuse.core.cxf.configuration.PolicyFortyFiveSecondsTimeoutEnpointConfigurer;
import it.rcs.fuse.core.cxf.configuration.PolicyNinetySecondsTimeoutEnpointConfigurer;
import it.rcs.fuse.core.cxf.configuration.PolicyOneHundredFiftySecondsTimeoutEnpointConfigurer;
import it.rcs.fuse.core.cxf.configuration.PolicyOneSecondTimeoutEnpointConfigurer;
import it.rcs.fuse.core.cxf.configuration.PolicyTenSecondsTimeoutEnpointConfigurer;
import it.rcs.fuse.core.cxf.configuration.PolicyThreeSecondsTimeoutEnpointConfigurer;
import it.rcs.fuse.core.cxf.configuration.PolicyTimeoutAndBasicAuthenticationEnpointConfigurer;
import it.rcs.fuse.core.dataformat.JSONDataFormat;
import it.rcs.fuse.core.route.BaseRouteBuilder;

public class Calls extends BaseRouteBuilder {

	public static final String TAG = "it.rcs.soa";
	public static final String TAG_DUMP = "it.rcs.soa_dump";
	
	
	@Override
	protected void components() {
		// TODO Auto-generated method stub
	}
	
	@Override
	protected void dataSources() {
		getContext().getDataFormats().put("it.rcs.soa.fuse_integration.bean.MyBean", new DataFormatDefinition(new JSONDataFormat(it.rcs.soa.fuse_integration.bean.MyBean.class)));		
	}
	
	@Override
	protected void resourceAdapters() throws Exception {
		getContext().getDataFormats().put("jsonDataFormat", new DataFormatDefinition(new JSONDataFormat()));		
	}
	
	@Override
	protected void policyEndpointConfigurer() {
		putOnRegistry(BaseRouteBuilder.policy1s, new PolicyOneSecondTimeoutEnpointConfigurer());
		putOnRegistry(BaseRouteBuilder.policy3s, new PolicyThreeSecondsTimeoutEnpointConfigurer());
		putOnRegistry(BaseRouteBuilder.policy5s, new PolicyFiveSecondsTimeoutEnpointConfigurer());
		putOnRegistry(BaseRouteBuilder.policy6s, new PolicyFiveSecondsTimeoutEnpointConfigurer());
		putOnRegistry(BaseRouteBuilder.policy7s, new PolicyTimeoutAndBasicAuthenticationEnpointConfigurer(7000));
		putOnRegistry(BaseRouteBuilder.policy8s, new PolicyTimeoutAndBasicAuthenticationEnpointConfigurer(8000));
		putOnRegistry(BaseRouteBuilder.policy9s, new PolicyTimeoutAndBasicAuthenticationEnpointConfigurer(9000));
		putOnRegistry(BaseRouteBuilder.policy10s, new PolicyTenSecondsTimeoutEnpointConfigurer());
		putOnRegistry(BaseRouteBuilder.policy15s, new PolicyFifteenSecondsTimeoutEnpointConfigurer());
		putOnRegistry(BaseRouteBuilder.policy45s, new PolicyFortyFiveSecondsTimeoutEnpointConfigurer());
		putOnRegistry(BaseRouteBuilder.policy90s, new PolicyNinetySecondsTimeoutEnpointConfigurer());
		putOnRegistry(BaseRouteBuilder.policy150s, new PolicyOneHundredFiftySecondsTimeoutEnpointConfigurer());
	}
	
	@Override
	protected void threadPoolDefinition() {
		// TODO Auto-generated method stub
	}

}
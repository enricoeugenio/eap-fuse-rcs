package it.rcs.soa.fuse_integration.route;


import java.util.Properties;

import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.jackson.JacksonDataFormat;
import org.apache.camel.component.properties.DefaultPropertiesParser;
import org.apache.camel.component.properties.PropertiesComponent;
import org.apache.camel.impl.CompositeRegistry;
import org.apache.camel.impl.DefaultCamelContext;
import org.apache.camel.impl.SimpleRegistry;
import org.apache.camel.model.DataFormatDefinition;
import org.apache.deltaspike.core.api.config.ConfigResolver;

import it.rcs.soa.fuse_integration.bean.MyBean;


public abstract class SoapBaseRoute extends RouteBuilder {

	
	public static final String policy1s = "policyTimeout1s";
	public static final String policy3s = "policyTimeout3s";
	public static final String policy5s = "policyTimeout5s";
	public static final String policy6s = "policyTimeout6s";
	public static final String policy7s = "policyTimeout7s";
	public static final String policy8s = "policyTimeout8s";
	public static final String policy9s = "policyTimeout9s";
	public static final String policy10s = "policyTimeout10s";
	public static final String policy15s = "policyTimeout15s";
	public static final String policy30s = "policyTimeout30s";
	public static final String policy45s = "policyTimeout45s";
	public static final String policy90s = "policyTimeout90s";
	public static final String policy150s = "policyTimeout150s";

	public static final String START = "START";
	public static final String STOP = "STOP";
	public static final String HTTP_RESPONSE_SYSTEM_FAULT = "417";
	public static final String HTTP_RESPONSE_BUSINESS_FAULT = "418";
	public static final String HTTP_RESPONSE_NOT_FOUND = "204";
	public static final String HTTP_RESPONSE_OK = "200";


	@Override
	public void configure() throws Exception {
		
		getContext().getDataFormats().put("jsonDataFormat", new DataFormatDefinition(new JacksonDataFormat()));
		
		
		JacksonDataFormat myBeanDataFormat = new JacksonDataFormat();
		myBeanDataFormat.setUnmarshalType((MyBean.class));
		getContext().getDataFormats().put("MyBean",	new DataFormatDefinition(myBeanDataFormat));
		
		PropertiesComponent component = getContext().getComponent("properties", PropertiesComponent.class);
		if (component == null) {
			component = new PropertiesComponent();
			component.setInitialProperties(org.apache.deltaspike.core.api.config.PropertyLoader
					.getProperties("/META-INF/apache-deltaspike.properties"));
			component.setPropertiesParser(new DeltaSpikeParser());
		} else {
			component.setInitialProperties(org.apache.deltaspike.core.api.config.PropertyLoader
					.getProperties("/META-INF/apache-deltaspike.properties"));
			component.setPropertiesParser(new DeltaSpikeParser());
		}
	}

	protected abstract void specialRouteConfigurer() throws Exception;

	protected void putOnRegistry(String key, Object value) {
		SimpleRegistry simpleRegistry = new SimpleRegistry();
		simpleRegistry.put(key, value);
		CompositeRegistry compositeRegistry = new CompositeRegistry();
		compositeRegistry.addRegistry(simpleRegistry);
		compositeRegistry.addRegistry(getContext().getRegistry());
		((DefaultCamelContext) getContext()).setRegistry(compositeRegistry);
	}


	protected boolean existsInRegistry(String value) {
		return getContext().getRegistry().lookupByName(value) != null;
	}

	protected abstract void threadPoolDefinition();

	protected abstract void policyEndpointConfigurer();

	protected abstract void headersSOAPConfigurer();

	protected abstract void components();

	protected abstract boolean isFirstTime();

	protected abstract void dataSources();

	protected abstract void resourceAdapters() throws Exception;

	class DeltaSpikeParser extends DefaultPropertiesParser {

		@Override
		public String parseProperty(String key, String value, Properties properties) {
			if (value == null) {
				return ConfigResolver.getPropertyValue(key);
			} else {
				return value;
			}

		}
	}

	protected void setupDeltaSpike() {
		if (getContext() != null) {
			PropertiesComponent component = getContext().getComponent("properties", PropertiesComponent.class);
			if (component == null) {
				component = new PropertiesComponent();
				component.setInitialProperties(org.apache.deltaspike.core.api.config.PropertyLoader
						.getProperties("/META-INF/apache-deltaspike.properties"));
				component.setPropertiesParser(new DeltaSpikeParser());
			} else {
				component.setInitialProperties(org.apache.deltaspike.core.api.config.PropertyLoader
						.getProperties("/META-INF/apache-deltaspike.properties"));
				component.setPropertiesParser(new DeltaSpikeParser());
			}
		}
	}

}

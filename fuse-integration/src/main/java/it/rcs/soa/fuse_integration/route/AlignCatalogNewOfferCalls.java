package it.rcs.soa.fuse_integration.route;

import javax.inject.Inject;

import org.apache.camel.LoggingLevel;

import it.rcs.enfinity.rcs_bc_soa.capi.soa.xsd.NewOfferResponse;
import it.rcs.soa.fuse_integration.bean.MyBean;
import it.rcs.soa.fuse_integration.predicate.VerificaHttp2xxResponseCodePredicate;
import it.rcs.soa.fuse_integration.predicate.VerificaResponseOkPredicate;

public class AlignCatalogNewOfferCalls extends Calls{
	public static final String GET_ALIGN_CATALOG_NEW_OFFER = "direct:getAlignCatalogNewOffer";
	public static final String ALIGN_CATALOG_NEW_OFFER = "direct:alignCatalogNewOfferWS";
	public static final String ALIGN_CATALOG_NEW_OFFER_RS = "direct:alignCatalogNewOfferRS";
	
	public static final String NEW_OFFER_RESPONSE_PROPERTY = "NewOfferResponseProperty";
	
	@Inject
	private VerificaResponseOkPredicate verificaResponseOkPredicate;
	@Inject
	private VerificaHttp2xxResponseCodePredicate verificaHttp2xxResponseCodePredicate;
	
	public void configure() throws Exception {
		super.configure();
		
		from(GET_ALIGN_CATALOG_NEW_OFFER)
			.routeId(GET_ALIGN_CATALOG_NEW_OFFER)
			.log(LoggingLevel.INFO, Calls.TAG, START)
			.errorHandler(noErrorHandler())
			.setProperty("Offer", body())
				.to(ALIGN_CATALOG_NEW_OFFER)
			.log(LoggingLevel.INFO, Calls.TAG, STOP)
		.end();
		
		from(ALIGN_CATALOG_NEW_OFFER)
			.routeId(ALIGN_CATALOG_NEW_OFFER)
			.log(LoggingLevel.INFO, Calls.TAG, START)
			.to("{{EP_WS_RCS}}")
			.choice()
				.when(verificaResponseOkPredicate)
					.convertBodyTo(MyBean.class)
					.to(ALIGN_CATALOG_NEW_OFFER_RS)
				.otherwise()
					.setProperty(NEW_OFFER_RESPONSE_PROPERTY, constant("KO"))
			.end()
			.choice()
				.when(verificaHttp2xxResponseCodePredicate)
					.setProperty(NEW_OFFER_RESPONSE_PROPERTY, constant("OK"))
				.otherwise()
					.setProperty(NEW_OFFER_RESPONSE_PROPERTY, constant("KO"))
			.end()
			.convertBodyTo(NewOfferResponse.class)
			.log(LoggingLevel.INFO, Calls.TAG, STOP)
		.end();
		
		from(ALIGN_CATALOG_NEW_OFFER_RS)
			.log(LoggingLevel.INFO, Calls.TAG, START)
			.errorHandler(noErrorHandler())
			.marshal("it.rcs.soa.fuse_integration.bean.MyBean")
			.log(LoggingLevel.INFO, Calls.TAG_DUMP, "AREA DATI INPUT")
			.to("{{EP_LOG_DUMP}}")
				.to("{{EP_RS}}")
			.unmarshal("it.rcs.soa.fuse_integration.bean.MyBean")
			.log(LoggingLevel.INFO, Calls.TAG_DUMP, "AREA DATI OUTPUT")
			.to("{{EP_LOG_DUMP}}")
			.log(LoggingLevel.INFO, Calls.TAG, STOP)
		.end();
			
	}

}

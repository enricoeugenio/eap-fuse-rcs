package it.rcs.soa.fuse_integration.route;

import javax.ejb.Startup;
import javax.enterprise.context.ApplicationScoped;

import org.apache.camel.Exchange;
import org.apache.camel.cdi.ContextName;

import it.rcs.enfinity.rcs_bc_soa.capi.soa.xsd.Offer;

@Startup
@ApplicationScoped
@ContextName("rcs-soa-context")
public class SoapConsumer extends Calls {

	public void configure() throws Exception {
		super.configure();
		
		onException(Exception.class)
	    	.log("Unexpected exception ${exception}")
	    .end();

		from("{{EP_WS_ALIGN_CATALOG_NEW_OFFER}}")
			.routeId("ws_align_catalog_new_offer_start")
			.setHeader(Exchange.HTTP_METHOD, constant("POST"))
			.inputType(Offer.class)
			.to(AlignCatalogNewOfferCalls.GET_ALIGN_CATALOG_NEW_OFFER)
			.removeHeaders("*")
			.setHeader(Exchange.CONTENT_TYPE, constant("application/xml"));
		
	}

}

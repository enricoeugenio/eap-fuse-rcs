package it.rcs.soa.fuse_integration.predicate;

import org.apache.camel.Exchange;
import org.apache.camel.Predicate;

import it.rcs.enfinity.rcs_bc_soa.capi.soa.xsd.NewOfferResponse;

public class VerificaResponseOkPredicate implements Predicate{

	@Override
	public boolean matches(Exchange exchange) {
		NewOfferResponse response = exchange.getIn().getBody(NewOfferResponse.class);
		return response!=null && response.getResponse()!=null && response.getResponse().equals("OK");
	}

}

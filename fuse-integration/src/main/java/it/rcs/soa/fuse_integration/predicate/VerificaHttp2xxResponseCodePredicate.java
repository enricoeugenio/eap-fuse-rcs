package it.rcs.soa.fuse_integration.predicate;

import org.apache.camel.Exchange;
import org.apache.camel.Predicate;

public class VerificaHttp2xxResponseCodePredicate implements Predicate {
	
	public enum HTTP_2XX_RESPONSE_CODE {
		HTTP_200("200"), HTTP_201("201"), HTTP_202("202"), HTTP_203("203"), HTTP_204("204"), HTTP_205("205"), HTTP_206("206"), HTTP_207("207");
	
		private String httpResponseCode;
		
		HTTP_2XX_RESPONSE_CODE(String httpResponseCode) {
			this.httpResponseCode = httpResponseCode;
		}
		
		public String getCode() {
			return this.httpResponseCode;
		}
		
		public static Boolean isHttp2xxResponseCode(String camelHttpResponseCode) {
			for (HTTP_2XX_RESPONSE_CODE http2xxResponseCode : HTTP_2XX_RESPONSE_CODE.values()) {
				if (http2xxResponseCode.httpResponseCode.equalsIgnoreCase(camelHttpResponseCode)) {
					return true;
				}
			}
			return false;
		}
	}
	
	@Override
	public boolean matches(Exchange exchange) {
		String camelHttpResponseCode = exchange.getIn().getHeader(Exchange.HTTP_RESPONSE_CODE, String.class);
		
		return HTTP_2XX_RESPONSE_CODE.isHttp2xxResponseCode(camelHttpResponseCode);
	}
	
}

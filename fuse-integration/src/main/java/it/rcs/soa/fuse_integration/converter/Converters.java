package it.rcs.soa.fuse_integration.converter;

import java.util.logging.Logger;

import org.apache.camel.Converter;
import org.apache.camel.Exchange;

import it.rcs.enfinity.rcs_bc_soa.capi.soa.xsd.NewOfferResponse;
import it.rcs.soa.fuse_integration.bean.MyBean;
import it.rcs.soa.fuse_integration.route.AlignCatalogNewOfferCalls;
import it.rcs.soa.fuse_integration.route.Calls;

public class Converters {
	
	private static final Logger logger = Logger.getLogger(Calls.TAG);

	@Converter
	public MyBean toObject(NewOfferResponse offerResponse, Exchange exchange) {
		logger.info("toObject.convertTo START");
		MyBean myBean = new MyBean();
		myBean.setMyField(offerResponse.getId());
		logger.info("toObject.convertTo STOP");
		return myBean;
	}
	
	@Converter
	public NewOfferResponse toStartResponse(MyBean myBean, Exchange exchange) {
		logger.info("toStartResponse.convertTo START");
		NewOfferResponse or = new NewOfferResponse();
		or.setResponse(exchange.getProperty(AlignCatalogNewOfferCalls.NEW_OFFER_RESPONSE_PROPERTY, String.class));
		logger.info("toStartResponse.convertTo STOP");
		return or;
	}
	
	

}

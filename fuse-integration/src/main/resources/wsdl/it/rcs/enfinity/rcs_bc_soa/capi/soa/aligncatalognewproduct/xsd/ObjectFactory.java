
package it.rcs.enfinity.rcs_bc_soa.capi.soa.aligncatalognewproduct.xsd;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;
import it.rcs.enfinity.rcs_bc_soa.capi.soa.displayinfo.xsd.DisplayInfo;
import it.rcs.enfinity.rcs_bc_soa.capi.soa.editorialentity.xsd.EditorialEntity;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the it.rcs.enfinity.rcs_bc_soa.capi.soa.aligncatalognewproduct.xsd package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _ProductContentId_QNAME = new QName("http://aligncatalognewproduct.soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", "contentId");
    private final static QName _ProductDisplayInfo_QNAME = new QName("http://aligncatalognewproduct.soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", "displayInfo");
    private final static QName _ProductEditorialEntity_QNAME = new QName("http://aligncatalognewproduct.soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", "editorialEntity");
    private final static QName _ProductRunaEntryPoint_QNAME = new QName("http://aligncatalognewproduct.soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", "runaEntryPoint");
    private final static QName _ProductSingleChannelProductTypeList_QNAME = new QName("http://aligncatalognewproduct.soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", "singleChannelProductTypeList");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: it.rcs.enfinity.rcs_bc_soa.capi.soa.aligncatalognewproduct.xsd
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Product }
     * 
     */
    public Product createProduct() {
        return new Product();
    }

    /**
     * Create an instance of {@link ArrayOfSingleChannelProduct }
     * 
     */
    public ArrayOfSingleChannelProduct createArrayOfSingleChannelProduct() {
        return new ArrayOfSingleChannelProduct();
    }

    /**
     * Create an instance of {@link SingleChannelProduct }
     * 
     */
    public SingleChannelProduct createSingleChannelProduct() {
        return new SingleChannelProduct();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://aligncatalognewproduct.soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", name = "contentId", scope = Product.class)
    public JAXBElement<String> createProductContentId(String value) {
        return new JAXBElement<String>(_ProductContentId_QNAME, String.class, Product.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DisplayInfo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://aligncatalognewproduct.soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", name = "displayInfo", scope = Product.class)
    public JAXBElement<DisplayInfo> createProductDisplayInfo(DisplayInfo value) {
        return new JAXBElement<DisplayInfo>(_ProductDisplayInfo_QNAME, DisplayInfo.class, Product.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EditorialEntity }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://aligncatalognewproduct.soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", name = "editorialEntity", scope = Product.class)
    public JAXBElement<EditorialEntity> createProductEditorialEntity(EditorialEntity value) {
        return new JAXBElement<EditorialEntity>(_ProductEditorialEntity_QNAME, EditorialEntity.class, Product.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://aligncatalognewproduct.soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", name = "runaEntryPoint", scope = Product.class)
    public JAXBElement<String> createProductRunaEntryPoint(String value) {
        return new JAXBElement<String>(_ProductRunaEntryPoint_QNAME, String.class, Product.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfSingleChannelProduct }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://aligncatalognewproduct.soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", name = "singleChannelProductTypeList", scope = Product.class)
    public JAXBElement<ArrayOfSingleChannelProduct> createProductSingleChannelProductTypeList(ArrayOfSingleChannelProduct value) {
        return new JAXBElement<ArrayOfSingleChannelProduct>(_ProductSingleChannelProductTypeList_QNAME, ArrayOfSingleChannelProduct.class, Product.class, value);
    }

}

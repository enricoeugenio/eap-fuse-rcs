
package it.rcs.enfinity.rcs_bc_soa.capi.soa.aligncatalognewproduct.xsd;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per SingleChannelProduct complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="SingleChannelProduct"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="prodId" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="prodVersion" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SingleChannelProduct", propOrder = {
    "prodId",
    "prodVersion"
})
public class SingleChannelProduct {

    @XmlElement(required = true)
    protected String prodId;
    @XmlElement(required = true)
    protected String prodVersion;

    /**
     * Recupera il valore della proprietÓ prodId.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProdId() {
        return prodId;
    }

    /**
     * Imposta il valore della proprietÓ prodId.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProdId(String value) {
        this.prodId = value;
    }

    /**
     * Recupera il valore della proprietÓ prodVersion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProdVersion() {
        return prodVersion;
    }

    /**
     * Imposta il valore della proprietÓ prodVersion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProdVersion(String value) {
        this.prodVersion = value;
    }

}

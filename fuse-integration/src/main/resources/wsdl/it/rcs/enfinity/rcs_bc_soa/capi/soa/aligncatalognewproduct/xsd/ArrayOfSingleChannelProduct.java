
package it.rcs.enfinity.rcs_bc_soa.capi.soa.aligncatalognewproduct.xsd;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per ArrayOfSingleChannelProduct complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfSingleChannelProduct"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="singleChannelProduct" type="{http://aligncatalognewproduct.soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd}SingleChannelProduct" maxOccurs="unbounded"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfSingleChannelProduct", propOrder = {
    "singleChannelProduct"
})
public class ArrayOfSingleChannelProduct {

    @XmlElement(required = true)
    protected List<SingleChannelProduct> singleChannelProduct;

    /**
     * Gets the value of the singleChannelProduct property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the singleChannelProduct property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSingleChannelProduct().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SingleChannelProduct }
     * 
     * 
     */
    public List<SingleChannelProduct> getSingleChannelProduct() {
        if (singleChannelProduct == null) {
            singleChannelProduct = new ArrayList<SingleChannelProduct>();
        }
        return this.singleChannelProduct;
    }

}

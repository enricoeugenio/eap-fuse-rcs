
package it.rcs.enfinity.rcs_bc_soa.capi.soa.aligncatalognewproduct.xsd;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;
import it.rcs.enfinity.rcs_bc_soa.capi.soa.displayinfo.xsd.DisplayInfo;
import it.rcs.enfinity.rcs_bc_soa.capi.soa.editorialentity.xsd.EditorialEntity;


/**
 * <p>Classe Java per Product complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="Product"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="contentId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="deviceMaxNum" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="displayInfo" type="{http://displayInfo.soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd}DisplayInfo" minOccurs="0"/&gt;
 *         &lt;element name="editorialEntity" type="{http://editorialEntity.soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd}EditorialEntity" minOccurs="0"/&gt;
 *         &lt;element name="inappId" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="multiChannelFlag" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="operatingSystem" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="productName" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="productPeriodicity" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="productType" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="productVersion" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="runaAppId" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="runaEntryPoint" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="sfdcProductId" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="singleChannelProductTypeList" type="{http://aligncatalognewproduct.soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd}ArrayOfSingleChannelProduct" minOccurs="0"/&gt;
 *         &lt;element name="supportedDigitalChannels" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="testata" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Product", propOrder = {
    "contentId",
    "deviceMaxNum",
    "displayInfo",
    "editorialEntity",
    "inappId",
    "multiChannelFlag",
    "operatingSystem",
    "productName",
    "productPeriodicity",
    "productType",
    "productVersion",
    "runaAppId",
    "runaEntryPoint",
    "sfdcProductId",
    "singleChannelProductTypeList",
    "supportedDigitalChannels",
    "testata"
})
public class Product {

    @XmlElementRef(name = "contentId", namespace = "http://aligncatalognewproduct.soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", type = JAXBElement.class, required = false)
    protected JAXBElement<String> contentId;
    protected int deviceMaxNum;
    @XmlElementRef(name = "displayInfo", namespace = "http://aligncatalognewproduct.soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", type = JAXBElement.class, required = false)
    protected JAXBElement<DisplayInfo> displayInfo;
    @XmlElementRef(name = "editorialEntity", namespace = "http://aligncatalognewproduct.soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", type = JAXBElement.class, required = false)
    protected JAXBElement<EditorialEntity> editorialEntity;
    @XmlElement(required = true)
    protected String inappId;
    @XmlElement(required = true)
    protected String multiChannelFlag;
    @XmlElement(required = true)
    protected String operatingSystem;
    @XmlElement(required = true)
    protected String productName;
    @XmlElement(required = true)
    protected String productPeriodicity;
    @XmlElement(required = true)
    protected String productType;
    @XmlElement(required = true)
    protected String productVersion;
    @XmlElement(required = true)
    protected String runaAppId;
    @XmlElementRef(name = "runaEntryPoint", namespace = "http://aligncatalognewproduct.soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", type = JAXBElement.class, required = false)
    protected JAXBElement<String> runaEntryPoint;
    @XmlElement(required = true)
    protected String sfdcProductId;
    @XmlElementRef(name = "singleChannelProductTypeList", namespace = "http://aligncatalognewproduct.soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfSingleChannelProduct> singleChannelProductTypeList;
    @XmlElement(required = true)
    protected String supportedDigitalChannels;
    @XmlElement(required = true)
    protected String testata;

    /**
     * Recupera il valore della proprietÓ contentId.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getContentId() {
        return contentId;
    }

    /**
     * Imposta il valore della proprietÓ contentId.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setContentId(JAXBElement<String> value) {
        this.contentId = value;
    }

    /**
     * Recupera il valore della proprietÓ deviceMaxNum.
     * 
     */
    public int getDeviceMaxNum() {
        return deviceMaxNum;
    }

    /**
     * Imposta il valore della proprietÓ deviceMaxNum.
     * 
     */
    public void setDeviceMaxNum(int value) {
        this.deviceMaxNum = value;
    }

    /**
     * Recupera il valore della proprietÓ displayInfo.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link DisplayInfo }{@code >}
     *     
     */
    public JAXBElement<DisplayInfo> getDisplayInfo() {
        return displayInfo;
    }

    /**
     * Imposta il valore della proprietÓ displayInfo.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link DisplayInfo }{@code >}
     *     
     */
    public void setDisplayInfo(JAXBElement<DisplayInfo> value) {
        this.displayInfo = value;
    }

    /**
     * Recupera il valore della proprietÓ editorialEntity.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link EditorialEntity }{@code >}
     *     
     */
    public JAXBElement<EditorialEntity> getEditorialEntity() {
        return editorialEntity;
    }

    /**
     * Imposta il valore della proprietÓ editorialEntity.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link EditorialEntity }{@code >}
     *     
     */
    public void setEditorialEntity(JAXBElement<EditorialEntity> value) {
        this.editorialEntity = value;
    }

    /**
     * Recupera il valore della proprietÓ inappId.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInappId() {
        return inappId;
    }

    /**
     * Imposta il valore della proprietÓ inappId.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInappId(String value) {
        this.inappId = value;
    }

    /**
     * Recupera il valore della proprietÓ multiChannelFlag.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMultiChannelFlag() {
        return multiChannelFlag;
    }

    /**
     * Imposta il valore della proprietÓ multiChannelFlag.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMultiChannelFlag(String value) {
        this.multiChannelFlag = value;
    }

    /**
     * Recupera il valore della proprietÓ operatingSystem.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOperatingSystem() {
        return operatingSystem;
    }

    /**
     * Imposta il valore della proprietÓ operatingSystem.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOperatingSystem(String value) {
        this.operatingSystem = value;
    }

    /**
     * Recupera il valore della proprietÓ productName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProductName() {
        return productName;
    }

    /**
     * Imposta il valore della proprietÓ productName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProductName(String value) {
        this.productName = value;
    }

    /**
     * Recupera il valore della proprietÓ productPeriodicity.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProductPeriodicity() {
        return productPeriodicity;
    }

    /**
     * Imposta il valore della proprietÓ productPeriodicity.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProductPeriodicity(String value) {
        this.productPeriodicity = value;
    }

    /**
     * Recupera il valore della proprietÓ productType.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProductType() {
        return productType;
    }

    /**
     * Imposta il valore della proprietÓ productType.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProductType(String value) {
        this.productType = value;
    }

    /**
     * Recupera il valore della proprietÓ productVersion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProductVersion() {
        return productVersion;
    }

    /**
     * Imposta il valore della proprietÓ productVersion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProductVersion(String value) {
        this.productVersion = value;
    }

    /**
     * Recupera il valore della proprietÓ runaAppId.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRunaAppId() {
        return runaAppId;
    }

    /**
     * Imposta il valore della proprietÓ runaAppId.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRunaAppId(String value) {
        this.runaAppId = value;
    }

    /**
     * Recupera il valore della proprietÓ runaEntryPoint.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getRunaEntryPoint() {
        return runaEntryPoint;
    }

    /**
     * Imposta il valore della proprietÓ runaEntryPoint.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setRunaEntryPoint(JAXBElement<String> value) {
        this.runaEntryPoint = value;
    }

    /**
     * Recupera il valore della proprietÓ sfdcProductId.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSfdcProductId() {
        return sfdcProductId;
    }

    /**
     * Imposta il valore della proprietÓ sfdcProductId.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSfdcProductId(String value) {
        this.sfdcProductId = value;
    }

    /**
     * Recupera il valore della proprietÓ singleChannelProductTypeList.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfSingleChannelProduct }{@code >}
     *     
     */
    public JAXBElement<ArrayOfSingleChannelProduct> getSingleChannelProductTypeList() {
        return singleChannelProductTypeList;
    }

    /**
     * Imposta il valore della proprietÓ singleChannelProductTypeList.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfSingleChannelProduct }{@code >}
     *     
     */
    public void setSingleChannelProductTypeList(JAXBElement<ArrayOfSingleChannelProduct> value) {
        this.singleChannelProductTypeList = value;
    }

    /**
     * Recupera il valore della proprietÓ supportedDigitalChannels.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSupportedDigitalChannels() {
        return supportedDigitalChannels;
    }

    /**
     * Imposta il valore della proprietÓ supportedDigitalChannels.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSupportedDigitalChannels(String value) {
        this.supportedDigitalChannels = value;
    }

    /**
     * Recupera il valore della proprietÓ testata.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTestata() {
        return testata;
    }

    /**
     * Imposta il valore della proprietÓ testata.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTestata(String value) {
        this.testata = value;
    }

}

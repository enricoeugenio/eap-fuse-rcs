
package it.rcs.enfinity.rcs_bc_soa.capi.soa.xsd;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import it.rcs.enfinity.rcs_bc_soa.capi.soa.aligncatalognewproduct.xsd.Product;


/**
 * <p>Classe Java per AlignCatalogNewProductRequest complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="AlignCatalogNewProductRequest"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="product" type="{http://aligncatalognewproduct.soa.capi.rcs_bc_soa.enfinity.rcs.it/xsd}Product"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AlignCatalogNewProductRequest", propOrder = {
    "product"
})
public class AlignCatalogNewProductRequest {

    @XmlElement(required = true)
    protected Product product;

    /**
     * Recupera il valore della proprietÓ product.
     * 
     * @return
     *     possible object is
     *     {@link Product }
     *     
     */
    public Product getProduct() {
        return product;
    }

    /**
     * Imposta il valore della proprietÓ product.
     * 
     * @param value
     *     allowed object is
     *     {@link Product }
     *     
     */
    public void setProduct(Product value) {
        this.product = value;
    }

}
